FROM maven:3.6-jdk-11 as build

ARG AWS_ACCESS_KEY_ID
ARG AWS_SECRET_ACCESS_KEY
ARG AWS_DEFAULT_REGION=us-east-1
ARG APP_ENV=local

ENV AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}
ENV AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
ENV AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION}
ENV APP_ENV=${APP_ENV}

WORKDIR /workspace/app

RUN apt-get update; \
    apt-get -y install groff; \
    curl --silent --show-error --fail "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"; \
    unzip awscliv2.zip; \
    ./aws/install

COPY src/main/docker/maven-settings.xml /root/.m2/settings.xml
COPY src/main/docker/jvm.config .
COPY ./pom.xml .
COPY ./src src

RUN export CODEARTIFACT_AUTH_TOKEN=$(aws codeartifact get-authorization-token --domain maxmilhas --domain-owner 516669511250 --query authorizationToken --output text); \
    mvn package -Dspring.profiles.active=${APP_ENV} -DskipTests

ARG DEPENDENCY=target/dependency
RUN mkdir -p ${DEPENDENCY} && (cd ${DEPENDENCY}; jar -xf ../*.jar)

FROM openjdk:11.0

ARG APP_ENV=local
ENV APP_ENV=${APP_ENV}
ENV spring_profiles_active=${APP_ENV}

#RUN adduser --system --group app
#USER app:app

ARG DEPENDENCY=/workspace/app/target/dependency
COPY --from=build ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY --from=build ${DEPENDENCY}/BOOT-INF/classes /app
COPY --from=build ${DEPENDENCY}/META-INF /app/META-INF

ENTRYPOINT ["java","-cp","app:app/lib/*","org.max.cart_service.CartServiceApplicationKt"]
@Library("jenkins-pipeline@master")

import br.com.maxmilhas.jenkins.Utils
import br.com.maxmilhas.jenkins.Pipeline

def pipe = new Pipeline('api-shopping-cart')

pipeline {
    agent any
    stages {
        stage('Register parameters') {
            steps {
                script {
                    pipe.setStackName(env.DEV_STACK ?: env.BRANCH)
                    pipe.setCommit(env.GIT_COMMIT)
                    pipe.setBranch(env.BRANCH)
                    pipe.setBuildUrl(env.BUILD_URL)
                }
            }
        }
        stage('Build docker image and push') {
            steps {
                script {
                    withAWS([credentials: 'homologation-ecr']) {
                        pipe.registerBuildArg("APP_ENV", "devstack")
                        pipe.registerBuildArg("AWS_ACCESS_KEY_ID", env.AWS_ACCESS_KEY_ID)
                        pipe.registerBuildArg("AWS_SECRET_ACCESS_KEY", env.AWS_SECRET_ACCESS_KEY)
                    }
                    pipe.setPathDockerfile("./Dockerfile")
                    pipe.buildImage(docker)
                }
            }
        }
        stage('Create stack on ECS') {
            steps {
                script {
                    pipe.createStack()
                    echo pipe.getApplicationEndpoint()
                }
            }
        }
        stage('Register Consul data') {
            steps {
                script {
                    def stackName = pipe.getStackName()
                    Utils.registerConsulData("devstack/${stackName}/API_CART_SERVICE", pipe.getApplicationEndpoint())
                }
            }
        }
    }
    post {
        always {
            deleteDir()
        }
        failure {
            script {
                pipe.notifyFailure((env.NOTIFICATION ?: '').tokenize(','))
            }
        }
        success {
            script {
                pipe.notifySuccess((env.NOTIFICATION ?: '').tokenize(','))
            }
        }
    }
}
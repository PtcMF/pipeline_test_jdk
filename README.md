
# E-commerce  - Cart Service

## Introdução

Este serviço fornece todos os contratos necessários para que os clientes realizem pedidos de oferta variados, tais como passagem de aérea, reserva de hotel, seguro viagem, etc.

## Instalação

### Pré-requisitos

* Docker
* JVM 11
* Maven
* AWS CLI version 2

### Download do código

```shell  
git clone git@bitbucket.org:maxmilhas/cart-service.git && cd cart-service
```  

### Dependências

Este projeto possui dependência com pacotes privados da MaxMilhas que, estão armazenados na AWS CodeArtifact. Para que o Maven consiga acessar esses pacotes, é necessário uma configuração local que informa ao Maven o endereço do repositório privado. Para tanto, crie o arquivo `~/.m2/settings.xml` contendo o seguinte conteúdo:

```xml  
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"   
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  
 xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 https://maven.apache.org/xsd/settings-1.0.0.xsd"> 
	<profiles> 
		 <profile> 
			 <id>maxmilhas-commons-java</id> 
			 <activation> 
				 <activeByDefault>true</activeByDefault> 
			 </activation> 
			 <repositories> 
				 <repository> 
					 <id>maxmilhas-commons-java</id> 
					 <url>https://maxmilhas-516669511250.d.codeartifact.us-east-1.amazonaws.com/maven/commons-java/</url> 
				 </repository>
			 </repositories> 
		 </profile> 
	</profiles> 
	<servers> 
		<server>
			<id>maxmilhas-commons-java</id> 
			<username>aws</username> 
			<password>${env.CODEARTIFACT_AUTH_TOKEN}</password> 
		</server>
	</servers>
</settings>  
```  

Repare que este arquivo assume que a variável de ambiente `CODEARTIFACT_AUTH_TOKEN` esteja presente  
na sessão atual do seu ambiente shell. Para criá-la, basta executar o seguinte comando:

```shell  
export CODEARTIFACT_AUTH_TOKEN=`aws codeartifact get-authorization-token --domain maxmilhas --domain-owner 516669511250 --query authorizationToken --output text`
```

Um ponto importante neste momento é que você tenha instalado e configurado o `AWS CLI version 2 ` na sua máquina e, se ainda não o tiver, siga as etapas descritas [aqui](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html) e [aqui](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html). Vale ressaltar também que **não** é necessário utilizar seu profile AWS de produção, embora os artefatos estejam armazenados naquela conta AWS.

Caso tenha dificuldades em gerar esta variável de ambiente, entre em contato com a equipe de infra-estrutura para ajuda-lo a entender e resolver o problema.

Uma vez criada a variável de ambiente, basta instalar as dependências do projeto utilizando o comando abaixo:

```shell  
mvn install
```

## Rodando a aplicação

Após a instalação da aplicação, o próximo passo é subir a aplicação em sua máquina. Para isso, suba os containers utilizando o comando a seguir:

	docker compose up -d

Dentre os serviços que serão criados, o mais importante é o banco de dados `mysql`, que estará disponível na porta `8102` da sua máquina. Isto feito, agora basta executar o projeto:

	mvn -Dspring.profiles.active=local -DskipTests=true package
	java -jar target/cart-service-*.jar --spring.profiles.active=local

Após a execução desses comandos, a aplicação estará disponível na porta `8101` do seu computador. Acesse o endereço `http://localhost:8101` para consumir as APIs deste serviço.

## Serviços

Após subir os containers através do comando `docker compose up -d` e aplicação, os seguintes serviços estarão disponíveis no seu computador:

| Serviço   | Porta | Descrição                        |
|-----------|-------|----------------------------------|
| api       | 8101  | Aplicação RESTful                |
| database  | 8102  | Banco de dados da aplicação      |
| prototype | 8103  | Protótipo navegável da aplicação |
| docs**    | 8104  | Documentação da aplicação.       |

## Documentação

O projeto conta com dois tipos de documentação: Swagger e Sphinx**

### Swagger

A documentação Swagger é gerada automaticamente e está disponível no endereço:

[http://localhost:8101/swagger-ui/index.html](http://localhost:8101/swagger-ui/index.html)

A documentação oficial, isto é, a que está publicada remotamente está disponível no endereço:

[http://docs.cart-service.maxilhas.com.br](http://docs.cart-service.maxilhas.com.br/swagger.html)**

### Sphinx**

A documentação contida no Sphinx possui automações para gerar automaticamente diagramas e o próprio Swagger. Esta documentação pode ser acessada no endereço:

[http://localhost:8104/index.html](http://localhost:8104/index.html)

A documentação oficial, isto é, a que está publicada remotamente está disponível no endereço:

[http://docs.cart-service.maxilhas.com.br](http://docs.cart-service.maxilhas.com.br)**


** Em desenvolvimento
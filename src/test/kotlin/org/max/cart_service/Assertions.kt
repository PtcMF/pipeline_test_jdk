package org.max.cart_service

import org.max.cart_service.asserts.CartAssert
import org.max.cart_service.asserts.MonetaryAmountAssert
import org.max.cart_service.domain.entity.Cart
import javax.money.MonetaryAmount

class Assertions {
    companion object {
        fun assertThat(cart: Cart): CartAssert = CartAssert(cart)
        fun assertThat(amount: MonetaryAmount): MonetaryAmountAssert = MonetaryAmountAssert(amount)
    }
}
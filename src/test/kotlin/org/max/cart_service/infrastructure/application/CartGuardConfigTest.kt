package org.max.cart_service.infrastructure.application

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.max.cart_service.application.guard.CartGuard
import org.max.cart_service.application.guard.CompositeGuard
import org.max.cart_service.domain.entity.Cart
import org.springframework.boot.autoconfigure.AutoConfigurations
import org.springframework.boot.test.context.runner.ApplicationContextRunner

internal class CartGuardConfigTest {
    private lateinit var contextRunner: ApplicationContextRunner

    @BeforeEach
    fun setUp() {
        contextRunner = ApplicationContextRunner()
            .withConfiguration(AutoConfigurations.of(CartGuardConfiguration::class.java))
    }

    @Test
    fun `It should create composite guard bean`() {
        contextRunner
            .run {
                Assertions.assertThat(it).hasBean("cartGuard")
                Assertions.assertThat(it).getBean("cartGuard").isInstanceOf(CompositeGuard::class.java)
            }
    }

    @Test
    fun `It should add a child to composite guard`() {
        contextRunner
            .withBean(FooGuard::class.java)
            .run {
                val guard = it.getBean("cartGuard", CompositeGuard::class.java)
                Assertions.assertThat(guard.hasGuard()).isTrue()
            }
    }
}

class FooGuard : CartGuard {
    override fun validate(cart: Cart) {
    }
}
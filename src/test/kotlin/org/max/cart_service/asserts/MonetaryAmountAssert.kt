package org.max.cart_service.asserts

import org.assertj.core.api.AbstractAssert
import org.javamoney.moneta.Money
import javax.money.MonetaryAmount

class MonetaryAmountAssert (actual: MonetaryAmount) : AbstractAssert<MonetaryAmountAssert, MonetaryAmount>(actual, MonetaryAmountAssert::class.java) {

    fun isInTheCurrency(currency: String): MonetaryAmountAssert {
        isNotNull

        if (actual.currency.currencyCode != currency) {
            failWithMessage("Expected that monetary amount is in the currency ${currency}, but it is in ${actual.currency.currencyCode}")
        }

        return this
    }

    fun isEqualTo(amount: Number): MonetaryAmountAssert {
        isNotNull

        if (!actual.isEqualTo(Money.of(amount, actual.currency))) {
            failWithMessage("Expected that the monetary amount is equal to $amount, but its current value is ${actual.number}")
        }

        return this
    }
}
package org.max.cart_service.asserts

import org.max.cart_service.domain.entity.Cart
import org.assertj.core.api.AbstractAssert

class CartAssert (actual: Cart) : AbstractAssert<CartAssert, Cart>(actual, CartAssert::class.java) {

    fun isEmpty(): CartAssert {
        isNotNull

        if (actual.offers.isNotEmpty()) {
            failWithMessage("Expected that the cart is empty, but it has ${actual.offers.size} offers")
        }

        return this
    }

    fun isNotEmpty(): CartAssert {
        isNotNull

        if (actual.offers.isEmpty()) {
            failWithMessage("Expected that the cart is not empty, but it is")
        }

        return this
    }

    fun hasMetadata(key: String): CartAssert {
        isNotNull

        if (!actual.metadata.containsKey(key)) {
            failWithMessage("Expected that the cart has the metadata key \"$key\", but it does not have")
        }

        return this
    }

    fun hasMetadataEqualsTo(key: String, value: Any): CartAssert {
        isNotNull

        if (!actual.metadata.containsKey(key)) {
            failWithMessage("Expected that the cart has the metadata key \"$key\", but it does not have")
        }

        if (actual.get("key")!! != value) {
            failWithMessage("Expected that the the metadata with key \"$key\" is equal to to $value, but it was not")
        }

        return this
    }
}
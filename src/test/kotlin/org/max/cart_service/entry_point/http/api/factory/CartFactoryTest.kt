package org.max.cart_service.entry_point.http.api.factory

import com.fasterxml.jackson.databind.ObjectMapper
import org.max.cart_service.domain.entity.Cart
import org.max.cart_service.domain.entity.CartAdjustment
import org.assertj.core.api.Assertions.*
import org.javamoney.moneta.Money
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

internal class CartFactoryTest {

    lateinit var subject: CartFactory

    @BeforeEach
    fun setup() {
        val monetaryAmountFactory = MonetaryAmountFactory()
        val adjustmentFactory = CartAdjustmentFactory(monetaryAmountFactory)
        subject = CartFactory(
            offerFactory = CartOfferFactory(monetaryAmountFactory, adjustmentFactory, ObjectMapper()),
            adjustmentFactory = CartAdjustmentFactory(monetaryAmountFactory),
            monetaryAmount = monetaryAmountFactory
        )
    }

    @Test
    fun `It can create a cart resource`() {
        val cart = Cart(number = "123")

        val expiration = LocalDateTime.now().plusHours(1)

        cart.add(
            sku = "123",
            baseAmount = Money.of(100, "BRL"),
            adjustmentAmount = Money.of(20, "BRL"),
            totalAmount = Money.of(120, "BRL"),
            adjustments = listOf(CartAdjustment(
                code = "FEE_SERVICE",
                amount = Money.of(20, "BRL")
            )),
            availabilityEnds = expiration,
            products = "[]",
        )

        val resource = subject.create(cart)

        // cart assertions
        assertThat(resource.number).isEqualTo("123")
        assertThat(resource.baseAmount).isEqualTo(100)
        assertThat(resource.adjustmentAmount).isEqualTo(20)
        assertThat(resource.totalAmount).isEqualTo(120)
//      assertThat(resource.expiresAt).isEqualTo(expiration.format(DateTimeFormatter.ISO_DATE_TIME))

        // offer assertions
        assertThat(resource.offers).size().isEqualTo(1)
        assertThat(resource.offers[0].sku).isEqualTo("123")
        assertThat(resource.offers[0].baseAmount).isEqualTo(100)
        assertThat(resource.offers[0].adjustmentAmount).isEqualTo(20)
        assertThat(resource.offers[0].totalAmount).isEqualTo(120)
        assertThat(resource.offers[0].availabilityEnds).isEqualTo(expiration.format(DateTimeFormatter.ISO_DATE_TIME))

        // adjustment assertions
        assertThat(resource.adjustments).size().isEqualTo(1)
        assertThat(resource.adjustments[0].code).isEqualTo("FEE_SERVICE")
        assertThat(resource.adjustments[0].amount).isEqualTo(20)
    }
}
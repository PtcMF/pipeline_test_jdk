package org.max.cart_service.entry_point.http.api.factory

import org.assertj.core.api.Assertions
import org.javamoney.moneta.Money
import org.junit.jupiter.api.Test

internal class MonetaryAmountFactoryTest {

    private val subject = MonetaryAmountFactory()

    @Test
    fun `It can create a simple data from monetary amount`() {
        val amount = Money.of(100, "BRL")
        Assertions.assertThat(subject.create(amount)).isEqualTo(100)
    }
}
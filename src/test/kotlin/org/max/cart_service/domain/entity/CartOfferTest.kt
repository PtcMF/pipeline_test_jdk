package org.max.cart_service.domain.entity

import org.max.cart_service.domain.exception.CartOfferSchemaNotFoundException
import org.max.cart_service.domain.repository.CartRepository
import org.assertj.core.api.Assertions
import org.javamoney.moneta.Money
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import java.time.LocalDateTime

@DataJpaTest
class CartOfferTest @Autowired constructor(
    val carts: CartRepository
) {

    @Test
    fun `It should check if the offer is not expired`() {
        val cart = Cart(number = "123")
        val offer = cart.add(
            sku = "XPTO",
            baseAmount = Money.of(10, "BRL"),
            adjustmentAmount = Money.of(10, "BRL"),
            totalAmount = Money.of(10, "BRL"),
            products = "[]",
            availabilityEnds = LocalDateTime.now().plusHours(1)
        )

        Assertions.assertThat(offer.isExpired()).isFalse()
    }

    @Test
    fun `It should check if the offer is expired`() {
        val offer = CartOffer(
            cart = Cart(number = "123"),
            sku = "XPTO",
            baseAmount = Money.of(10, "BRL"),
            adjustmentAmount = Money.of(10, "BRL"),
            totalAmount = Money.of(10, "BRL"),
            products = "[]",
            availabilityEnds = LocalDateTime.now().minusHours(1)
        )

        Assertions.assertThat(offer.isExpired()).isTrue()
    }

    @Test
    fun `It should accept a list of schemas`() {
        val cart = Cart(number = "123")

        cart.add(
            sku = "XPTO",
            baseAmount = Money.of(10, "BRL"),
            adjustmentAmount = Money.of(10, "BRL"),
            totalAmount = Money.of(10, "BRL"),
            products = "[]",
            schemas = listOf(CartOfferSchema(
                name = "PassengerList",
                definition = "{}",
                data = "{}"
            ))
        )

        carts.save(cart)

        val found = carts.findOneByNumber("123")
        val offer = found!!.find("XPTO")

        Assertions.assertThat(offer.schemas).size().isEqualTo(1)
    }

    @Test
    fun `It should be able to set data to offer's schema`() {
        val cart = Cart(number = "123")

        cart.add(
            sku = "XPTO",
            baseAmount = Money.of(10, "BRL"),
            adjustmentAmount = Money.of(10, "BRL"),
            totalAmount = Money.of(10, "BRL"),
            products = "[]",
            schemas = mutableListOf(CartOfferSchema(
                name = "PassengerList",
                definition = "{}"
            ))
        )

        carts.save(cart)

        val found = carts.findOneByNumber("123")
        val offer = found!!.find("XPTO")
        offer.setSchemaData(10, "{\"foo\":\"bar\"}")
        carts.save(cart)

        Assertions.assertThat(offer.getSchemaData(10)).isEqualTo("{\"foo\":\"bar\"}")
    }

    @Test
    fun `It should not allow requesting a non existing schema from offer`() {
        val cart = Cart(number = "123")

        cart.add(
            sku = "XPTO",
            baseAmount = Money.of(10, "BRL"),
            adjustmentAmount = Money.of(10, "BRL"),
            totalAmount = Money.of(10, "BRL"),
            products = "[]",
            schemas = mutableListOf(CartOfferSchema(
                name = "PassengerList",
                definition = "{}"
            ))
        )

        carts.save(cart)

        val found = carts.findOneByNumber("123")

        assertThrows<CartOfferSchemaNotFoundException> {
            val offer = found!!.find("XPTO")
            offer.getSchemaData(999)
        }
    }

    @Test
    fun `It should be able to unset from offer's schema`() {
        val cart = Cart(number = "123")

        cart.add(
            sku = "XPTO",
            baseAmount = Money.of(10, "BRL"),
            adjustmentAmount = Money.of(10, "BRL"),
            totalAmount = Money.of(10, "BRL"),
            products = "[]",
            schemas = mutableListOf(CartOfferSchema(
                name = "PassengerList",
                definition = "{}"
            ))
        )

        carts.save(cart)

        val found = carts.findOneByNumber("123")
        val offer = found!!.find("XPTO")
        offer.setSchemaData(10, "{\"foo\":\"bar\"}")
        carts.save(cart)

        offer.unsetSchemaData(10)

        carts.save(cart)

        Assertions.assertThat(offer.getSchemaData(10)).isNull()
    }
}
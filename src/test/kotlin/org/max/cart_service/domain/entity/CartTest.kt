package org.max.cart_service.domain.entity

import org.max.cart_service.domain.exception.CartNotFoundException
import org.max.cart_service.domain.exception.CartOfferExpiredException
import org.max.cart_service.domain.repository.CartRepository
import org.max.cart_service.Assertions.Companion.assertThat
import org.max.cart_service.domain.exception.CartOfferDuplicatedException
import org.assertj.core.api.Assertions.*
import org.javamoney.moneta.Money
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import java.time.LocalDateTime

@DataJpaTest
class CartTest @Autowired constructor(
    val carts: CartRepository
){
    @Test
    fun `It should initialize with BRL currency`() {
        // setup
        val cart = Cart(number = "XPTO")
        assertThat(cart.currency).isEqualTo("BRL")
    }

    @Test
    fun `It should accept a arbitrary data set as metadata`() {
        val cart = Cart(number = "XPTO")
        cart.set("foo", "bar")
        carts.save(cart)
        assertThat(cart).hasMetadataEqualsTo("foo", "bar")
    }

    @Test
    fun `It should create a cart with its amounts zeroed`() {
        // setup
        val cart = Cart(number = "XPTO")
        carts.save(cart)

        // assertions
        val found = carts.findOneById(cart.id)
        assertThat(found!!.baseAmount).isEqualTo(Money.of(0, "BRL"))
        assertThat(found.adjustmentAmount).isEqualTo(Money.of(0, "BRL"))
        assertThat(found.totalAmount).isEqualTo(Money.of(0, "BRL"))
    }

    @Test
    fun `It should create an empty cart`() {
        // setup
        carts.save(Cart(number = "XPTO"))
        val cart = carts.findOneByNumber("XPTO")

        // assertions
        assertThat(cart!!.offers).isEmpty()
    }

    @Test
    fun `It should be able to add an offer to the cart`() {
        // setup
        val cart = Cart(number = "XPTO")
        cart.add(
            sku ="OTPX",
            baseAmount = Money.of(100, "BRL"),
            adjustmentAmount = Money.of(20, "BRL"),
            totalAmount = Money.of(120, "BRL"),
            products = "[]",
        )
        carts.save(cart)

        val found = carts.findOneByNumber("XPTO")

        // assertions
        assertThat(found!!.offers).size().isEqualTo(1)
        assertThat(found.count()).isEqualTo(1)
        assertThat(found.baseAmount).isEqualTo(Money.of(100, "BRL"))
        assertThat(found.adjustmentAmount).isEqualTo(Money.of(20, "BRL"))
        assertThat(found.totalAmount).isEqualTo(Money.of(120, "BRL"))
    }

    @Test
    fun `It should not allow adding an expired offer to the cart`() {
        // setup
        val cart = Cart(number = "XPTO")

        assertThrows<CartOfferExpiredException> {
            cart.add(
                sku ="OTPX",
                baseAmount = Money.of(100, "BRL"),
                adjustmentAmount = Money.of(20, "BRL"),
                totalAmount = Money.of(120, "BRL"),
                availabilityEnds = LocalDateTime.now().minusHours(1),
                products = "[]",
            )
        }
    }

    @Test
    fun `It should not allow adding the same offer twice`() {
        // setup
        val cart = Cart(number = "XPTO")

        cart.add(
            sku ="OTPX",
            baseAmount = Money.of(100, "BRL"),
            adjustmentAmount = Money.of(20, "BRL"),
            totalAmount = Money.of(120, "BRL"),
            products = "[]",
        )

        assertThrows<CartOfferDuplicatedException> {
            cart.add(
                sku ="OTPX",
                baseAmount = Money.of(100, "BRL"),
                adjustmentAmount = Money.of(20, "BRL"),
                totalAmount = Money.of(120, "BRL"),
                products = "[]",
            )
        }
    }

    @Test
    fun `It can find an offer by its SKU`() {
        // setup
        val cart = Cart(number = "XPTO")
        cart.add(
            sku ="OTPX",
            baseAmount = Money.of(100, "BRL"),
            adjustmentAmount = Money.of(20, "BRL"),
            totalAmount = Money.of(120, "BRL"),
            products = "[]",
        )
        carts.save(cart)

        val offer = cart.find("OTPX")

        assertThat(offer).isInstanceOf(CartOffer::class.java)
    }

    @Test
    fun `It can check whether an offer is present in the cart or not`() {
        // setup
        val cart = Cart(number = "XPTO")
        assertThat(cart.has("OTPX")).isFalse()

        cart.add(
            sku ="OTPX",
            baseAmount = Money.of(100, "BRL"),
            adjustmentAmount = Money.of(20, "BRL"),
            totalAmount = Money.of(120, "BRL"),
            products = "[]",
        )
        carts.save(cart)

        assertThat(cart.has("OTPX")).isTrue()
    }

    @Test
    fun `It should be able to remove an offer from the cart`() {
        // setup
        val cart = Cart(number = "XPTO")
        cart.add(
            sku ="OTPX",
            baseAmount = Money.of(100, "BRL"),
            adjustmentAmount = Money.of(20, "BRL"),
            totalAmount = Money.of(120, "BRL"),
            products = "[]",
        )
        carts.save(cart)

        val found = carts.findOneByNumber("XPTO")
        found!!.remove("OTPX")
        carts.save(found)

        // assertions
        assertThat(found.offers).size().isEqualTo(0)
        assertThat(found.baseAmount).isEqualTo(Money.of(0, "BRL"))
        assertThat(found.adjustmentAmount).isEqualTo(Money.of(0, "BRL"))
        assertThat(found.totalAmount).isEqualTo(Money.of(0, "BRL"))
    }

    @Test
    fun `It can add a promotion adjustment to the cart `() {
        // setup
        val cart = Cart(number = "XPTO")
        cart.addAdjustment(Money.of(-20, "BRL"), "PROMOTION")
        carts.save(cart)

        val found = carts.findOneByNumber("XPTO")

        // assertions
        assertThat(found!!.baseAmount).isEqualTo(Money.of(0, "BRL"))
        assertThat(found.adjustmentAmount).isEqualTo(Money.of(-20, "BRL"))
        assertThat(found.totalAmount).isEqualTo(Money.of(-20, "BRL"))
        assertThat(found.adjustments).size().isEqualTo(1)
    }

    @Test
    fun `It should compute adjustments from offer`() {
        // setup
        val cart = Cart(number = "XPTO")
        cart.add(
            sku = "OTPX",
            baseAmount = Money.of(100, "BRL"),
            adjustmentAmount = Money.of(-20, "BRL"),
            totalAmount = Money.of(80, "BRL"),
            adjustments = listOf(CartAdjustment(
                code = "PROMOTION",
                amount = Money.of(-20, "BRL")
            )),
            products = "[]",
        )
        carts.save(cart)

        val found = carts.findOneByNumber("XPTO")

        // assertions
        assertThat(found!!.baseAmount).isEqualTo(Money.of(100, "BRL"))
        assertThat(found.adjustmentAmount).isEqualTo(Money.of(-20, "BRL"))
        assertThat(found.totalAmount).isEqualTo(Money.of(80, "BRL"))
        assertThat(found.adjustments).size().isEqualTo(1)
    }

    @Test
    fun `It should remove an adjustment if its correspondent offer is removed from cart`() {
        // setup
        val cart = Cart(number = "XPTO")
        cart.add(
            sku = "OTPX",
            baseAmount = Money.of(100, "BRL"),
            adjustmentAmount = Money.of(-20, "BRL"),
            totalAmount = Money.of(80, "BRL"),
            adjustments = listOf(CartAdjustment(
                code = "PROMOTION",
                amount = Money.of(-20, "BRL")
            )),
            products = "[]",
        )
        carts.save(cart)

        val found = carts.findOneByNumber("XPTO")
        found!!.remove("OTPX")
        carts.save(cart)

        // assertions
        assertThat(found.baseAmount).isEqualTo(Money.of(0, "BRL"))
        assertThat(found.adjustmentAmount).isEqualTo(Money.of(0, "BRL"))
        assertThat(found.totalAmount).isEqualTo(Money.of(0, "BRL"))
        assertThat(found.adjustments).size().isEqualTo(0)
    }
}

package org.max.cart_service.domain.repository

import org.max.cart_service.domain.entity.Cart
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager

@DataJpaTest
class CartRepositoryTest @Autowired constructor(
    val entityManager: TestEntityManager,
    val carts: CartRepository
) {

    @Test
    fun `It should find a cart by its ID`() {
        val cart = Cart(number = "XPTO")
        entityManager.persist(cart)
        entityManager.flush()
        val found = carts.findOneById(cart.id)
        Assertions.assertThat(found).isEqualTo(cart)
    }

    @Test
    fun `It should find a cart by its number`() {
        val cart = Cart(number = "XPTO")
        entityManager.persist(cart)
        entityManager.flush()
        val found = carts.findOneByNumber("XPTO")
        Assertions.assertThat(found).isEqualTo(cart)
    }
}

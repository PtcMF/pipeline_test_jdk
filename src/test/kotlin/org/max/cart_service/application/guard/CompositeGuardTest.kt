package org.max.cart_service.application.guard

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.max.cart_service.domain.entity.Cart
import org.mockito.kotlin.doNothing
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

internal class CompositeGuardTest {

    @Test
    fun `It should call children guards`() {

        // Given
        val cart:Cart = mock {  }
        val guard:CartGuard = mock {
        }

        // When
        val composite = CompositeGuard(listOf(guard))
        doNothing().`when`(guard).validate(cart)
        composite.validate(cart)

        // Then
        verify(guard).validate(cart)
    }
}
package org.max.cart_service.application.factory

import org.max.cart_service.Assertions.Companion.assertThat
import org.max.cart_service.application.client.OfferClient
import org.max.cart_service.application.command.CreateCart
import org.max.cart_service.application.model.CartAdjustment
import org.max.cart_service.application.model.CartOffer
import org.max.cart_service.application.model.CartOfferSchema
import org.max.cart_service.application.number_generator.NumberGenerator
import org.javamoney.moneta.Money
import org.junit.jupiter.api.Test
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import java.time.LocalDateTime

internal class CartFactoryTest {

    @Test
    fun `It can create an empty cart`() {

        val numberGenerator: NumberGenerator = mock {
            on { generate() } doReturn "XPTO"
        }

        val offerClient: OfferClient = mock()
        val subject = CartFactory(numberGenerator, offerClient)
        val cart = subject.create(mock())
        assertThat(cart).isEmpty()
    }

    @Test
    fun `It can create a cart containing offers`() {

        val offer: CartOffer = mock {
            on { sku } doReturn "123"
            on { baseAmount } doReturn Money.of(100, "BRL")
            on { adjustmentAmount } doReturn Money.of(20, "BRL")
            on { totalAmount } doReturn Money.of(120, "BRL")
            on { adjustments } doReturn listOf(CartAdjustment("FEE", Money.of(20, "BRL")))
            on { schemas } doReturn listOf(CartOfferSchema("GuestList", "{}"))
            on { availabilityEnds } doReturn LocalDateTime.now().plusHours(1)
            on { products } doReturn "[]"
        }

        val numberGenerator: NumberGenerator = mock {
            on { generate() } doReturn "XPTO"
        }

        val offerClient: OfferClient = mock {
            on { find("123") } doReturn offer
        }

        val subject = CartFactory(numberGenerator, offerClient)
        val command: CreateCart = mock {
            on { offers } doReturn listOf(CartOffer(sku = "123"))
        }
        val cart = subject.create(command)
        assertThat(cart).isNotEmpty()
        assertThat(cart.baseAmount).isInTheCurrency("BRL")
        assertThat(cart.baseAmount).isEqualTo(100)
    }
}
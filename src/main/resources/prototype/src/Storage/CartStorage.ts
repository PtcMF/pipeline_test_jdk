import Cookies from 'js-cookie';

export interface CartStorage {
    read(): Promise<string>;
    write(id: string): Promise<void>;
    has(): Promise<boolean>;
    clear(): Promise<void>;
}

export class CookieCartStorage implements CartStorage {
    private static COOKIE_NAME = 'cartId';

    has(): Promise<boolean> {
        return Promise.resolve(Cookies.get(CookieCartStorage.COOKIE_NAME) !== undefined);
    }

    read(): Promise<string> {
        const id = Cookies.get(CookieCartStorage.COOKIE_NAME);

        if (undefined === id) {
            throw new Error('There is no cart id saved on cookie');
        }

        return Promise.resolve(id);
    }

    write(id: string): Promise<void> {
        Cookies.set(CookieCartStorage.COOKIE_NAME, id);
        return Promise.resolve();
    }

    clear(): Promise<void> {
        Cookies.remove(CookieCartStorage.COOKIE_NAME);
        return Promise.resolve(undefined);
    }
}
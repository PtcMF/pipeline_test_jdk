import {Delete, FlightSharp, Hotel, HotelSharp, ShoppingBasket} from "@material-ui/icons";
import {Button, Grid, IconButton, Typography} from "@material-ui/core";
import React, {FunctionComponent} from "react";
import CartStore from "./Store/CartStore";
import {inject, observer} from "mobx-react";

type CartSummaryProps = {
    cartStore?: CartStore
}

type dictionary = {
    [key: string]: string
};

const translate = function(key: string) {
    const keyVal: dictionary = {
        FEE_SERVICE: 'Taxa de serviço',
        TAX: 'Taxa de serviço'
    }

    return keyVal[key] ?? key;
};

const formatCurrency = (value: number) => new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(value);

const CartSummary:FunctionComponent<CartSummaryProps> = ({cartStore}) => <div className='cart-container'>
    <div className='cart card'>
        <div className='heading'>
            <ShoppingBasket />
            <strong>Resumo</strong> ({cartStore!.count} items)
        </div>

        {cartStore!.items.length === 0 && <>
            <Typography variant="subtitle2">Seu carrinho está vazio</Typography>
        </>}

        {cartStore!.items.map((item, index) =>
            <div key={index} className='cart-flight'>
                { item.products[0].type === "@Airfare" && <FlightSharp />}
                { item.products[0].type === "@Accommodation" && <HotelSharp />}
                <div className='cart-flight-info'>
                    <div className='cart-flight-info-title'>
                        <strong>{item.products[0].seller.name}</strong>
                    </div>
                    <div className='cart-flight-info-title'>
                        {item.products[0].description}
                    </div>

                    <div className='cart-flight-info-price'>
                        <strong>{formatCurrency(item.totalAmount / 100)}</strong>
                    </div>
                </div>

                <div className='cart-flight-action'>
                    <IconButton aria-label="remove from cart" onClick={() => cartStore!.removeItem(item.sku)}>
                        <Delete />
                    </IconButton>
                </div>
            </div>
        )}

        <footer>
            <div><strong>Subtotal:</strong> {formatCurrency(Number(cartStore!.subtotal))}</div>
            {cartStore!.adjustments.map(adj => {
                return <div><strong>{translate(adj.code)}:</strong> {formatCurrency(Number(adj.amount / 100))}</div>
            })}
            <div><strong>Total:</strong> {formatCurrency(Number(cartStore!.total))}</div>
        </footer>
    </div>
</div>

export default inject('cartStore')(observer(CartSummary));
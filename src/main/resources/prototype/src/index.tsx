import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { Provider } from "mobx-react";
import CartStore from './Store/CartStore';
import reportWebVitals from './reportWebVitals';
import "@fontsource/roboto";
import {CartApi} from "./Api/Cart";
import {CookieCartStorage} from "./Storage/CartStorage";
import UiStore from "./Store/UiStore";

const cartStore = new CartStore(new CartApi(), new CookieCartStorage());
const uiStore = new UiStore();

ReactDOM.render(
  <React.StrictMode>
      <Provider cartStore={cartStore} uiStore={uiStore}>
        <App />
      </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

import superagent from 'superagent';
import {JSONSchema7} from "json-schema";

const baseUrl: string = 'http://localhost:8080/';

export type OfferList<T> = Array<Offer<T>>;

export type Product = {
    description: string;
    additionalType: string;
    schemas: Array<JSONSchema7>;
    // itinerary: ItemList<T>;
};

export type Airport = {
    name: string;
    iataCode: string;
}

export type Flight = {
    seller: {
        name: string;
        iataCode: string;
    };
    flightNumber: string;
    departureAirport: Airport;
    departureTime: Date;
    arrivalAirport: Airport;
    arrivalTime: Date;
    estimatedFlightDuration: string;
    itinerary: Array<Flight>;
};

export type Accommodation = {
    description: String,
    seller: LodgingBusiness
};

export type LodgingBusiness = {
    name: string,
    photo: ImageObject,
    starRating: Rating,
    aggregateRating: AggregateRating,
    address: PostalAddress
}

export type PostalAddress = {
    addressCountry: string,
    addressState: string,
    addressCity: string,
    addressNeighborhood: string,
    streetAddress: string,
    postalCode: string,
    addressComplement?: string,
}

export type ImageObject = {
    width: number,
    height: number,
    url: string
}

export type Rating = {
    ratingValue: number
}

export type AggregateRating = Rating & {
    reviewCount: number
}

export type PriceSpecification = {
    price: number;
    currency: string;
};

export type Offer<T> = {
    sku: string,
    description: string
    products: Array<T>
    baseAmount: PriceSpecification,
    adjustmentsAmount: PriceSpecification,
    totalAmount: PriceSpecification
}

export class OfferApi {

    private agent;

    constructor() {
        this.agent = superagent.agent();
    }

    async findOffers <T> (business: string): Promise<OfferList<T>> {
        const result  = await this.agent
            .get(`${baseUrl}/offers`)
            .query({business: business});

        return result.body
    }

    async findAirfareOffers(): Promise<OfferList<Flight>> {
        return this.findOffers<Flight>("airfare")
    }

    async findLodgingOffers(): Promise<OfferList<Accommodation>> {
        return this.findOffers<Accommodation>("lodging")
    }
}

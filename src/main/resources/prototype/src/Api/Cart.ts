import superagent from 'superagent';
import {JSONSchema7} from "json-schema";

const baseUrl: string = 'http://localhost:8101/v1';

export type Cart = {
    id: string,
    number: string
    offers: Array<CartItem>,
    baseAmount: number,
    totalAmount: number,
    adjustmentsAmount: number,
    adjustments: Array<Adjustment>
}

export type CartItem = {
    id: string,
    sku: string,
    totalAmount: number,
    products: Array<Product>,
    schemas: Array<{
        name: string,
        schema: string
    }>
}

export type Product = {
    seller: Seller,
    description: string,
    type: string
}

export type Seller = {
    name: string
}

export type CartSchema = {
    id: number,
    name: string,
    definition: JSONSchema7,
    data: object
}

export type Adjustment = {
    code: string,
    amount: number
};

export class CartApi {

    private agent;

    constructor() {
        this.agent = superagent.agent();
    }

    async create(): Promise<string> {
        const result  = await this.agent
            .post(`${baseUrl}/carts`)
            .set("Content-Type", "application/json");
        return result.body.number
    }

    async details(cartId: string): Promise<Cart> {
        const result = await this.agent.get(`${baseUrl}/carts/${cartId}`);
        return result.body;
    }

    async addItem(cartId: string, itemId: string, business: string): Promise<void> {
        await this.agent
            .post(`${baseUrl}/carts/${cartId}/offers`)
            .query({business})
            .send({ sku: itemId });

        return Promise.resolve();
    }

    async removeItem(cartId: string, itemId: string): Promise<void> {
        await this.agent
            .delete(`${baseUrl}/carts/${cartId}/offers/${itemId}`);

        return Promise.resolve();
    }

    async getSchemas(cartId: string): Promise<Array<CartSchema>> {
        const result = await this.agent.get(`${baseUrl}/carts/${cartId}/schemas`)
        return result.body
    }

    async saveSchemaData(cartId: string, schemaId: number, data: any): Promise<boolean> {
        const result = await this.agent
            .put(`${baseUrl}/carts/${cartId}/schemas/${schemaId}`)
            .send(data)

        return result.body
    }

    async complete(cartId: string): Promise<boolean> {
        const result = await this.agent
            .put(`${baseUrl}/carts/${cartId}`)
            .set("Content-Type", "application/json")

        return result.status === 202
    }
}



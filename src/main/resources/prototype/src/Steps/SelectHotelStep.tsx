import { VFC, useState, useEffect } from "react";
import {Button } from "@material-ui/core";
import {inject, observer} from "mobx-react";
import CartStore from "../Store/CartStore";
import {Offer, OfferApi, OfferList, Accommodation} from "../Api/Offer";
import './style.scss';
import UiStore from "../Store/UiStore";
import { Rating } from "@material-ui/lab";

type SelectHotelStepProps = {
    cartStore?: CartStore,
    uiStore?: UiStore,
}

const SelectHotelStep:VFC<SelectHotelStepProps> = ({ cartStore, uiStore }) => {
    const [offers, setOffers] = useState<OfferList<Accommodation>>([])

    useEffect(() => {
        const getOffers = async () => {
            try {
                const offerApi = new OfferApi()
                uiStore!.loading = true
                const offersAux = await offerApi.findLodgingOffers()
                uiStore!.loading = false
                setOffers(offersAux)
            } catch (err) {
                console.error(err)
            }
        }
        getOffers()
    }, []);

    const selectOffer = async (offer: Offer<Accommodation>) => {
        uiStore!.loading = true
        await cartStore!.addItem(offer.sku, 'lodging');
        uiStore!.loading = false
        uiStore!.setStep(3);
    };

    return (
        <div className='flights'>
            
            {offers.map((offer: Offer<Accommodation>, index) =>
                <div className='flight-card card' key={`${offer.sku}-${index}`}>
                    <div style={{width: 150, backgroundSize: "cover", backgroundImage: `url(${offer.products[0].seller.photo.url})` }} />
                    <div className='flight-card-info'>
                        <div className='flight'>
                            <div className='flight-direction'>
                                <span>
                                    <strong>{offer.products[0].seller.name}</strong>
                                </span>
                            </div>
                            <div>
                                <Rating value={offer.products[0].seller.starRating.ratingValue} />
                                <div>{offer.products[0].seller.address.streetAddress}</div>
                            </div>
                        </div>
                    </div>
                    <div className='flight-card-select'>
                        <h5>{new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(offer.totalAmount.price / 100)}</h5>
                        <Button className="btn" onClick={() => selectOffer(offer) }>Selecionar</Button>
                    </div>
                </div>
            )}
        </div>
    )
}

export default inject('cartStore', 'uiStore')(observer(SelectHotelStep));

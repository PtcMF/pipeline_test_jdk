import React, { useEffect, useState, VFC } from "react";
import {inject, observer} from "mobx-react";
import CartStore from "../Store/CartStore";
import './payment.scss';
import Form from "@rjsf/material-ui";
import {JSONSchema7} from "json-schema";
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Modal } from "@material-ui/core";

type PaymentStepProps = {
    cartStore?: CartStore
}

const paymentFormData = {
  creditCard: {
    creditCardNumber: "5219842657713906",
    holderName:"John Doe",
    cvv: 213,
    expirationDate: "2024-10-24"
  }
}

const paymentFormSchema = {
    "type": "object",
    "properties": {
      "creditCard": {
        "title": "Cartão de crédito",
        "type": "object",
        "required": [
          "creditCardNumber",
          "holderName",
          "cvv",
          "expirationDate"
        ],
        "properties": {
          "creditCardNumber": {
            "title": "Número cartão de crédito",
            "type": "string"
          },
          "holderName": {
            "title": "Nome impresso no cartão",
            "type": "string"
          },
          "cvv": {
            "title": "Código de segurança",
            "type": "number"
          },
          "expirationDate": {
            "title": "Validade do cartão",
            "type": "string",
            "format": "date"
          }
        }
      },
      "transfer": {
        "title": "Transferência",
        "type": "object",
        "properties": {
          "bank": {
            "type": "string",
            "title": "Seu banco",
            "enum": [
              "Banco do Brasil",
              "Caixa econômica",
              "Itaú",
              "Bradesco"
            ]
          },
          "bankAccount": {
            "type": "string",
            "title": "Sua Conta",
            "example": "1111-1"
          }
        }
      }
    }
}

const PaymentStep:VFC<PaymentStepProps> = ({ cartStore }) => {
    
    const [open, setOpen] = useState(false)

    const handleClickOpen = () => {
      setOpen(true);
    };
  
    const handleClose = () => {
      setOpen(false);
    };

    const completeCart = async (formData: any): Promise<void> => {
        const result = await cartStore?.complete()
        if (result) {
          setOpen(true)
        }
    }

    return (
        <div className='card payment'>
            <Form
                schema={paymentFormSchema as JSONSchema7}
                onSubmit={async (ev) => await completeCart(ev.formData)}
                formData={paymentFormData}
            />
            <Dialog
              open={open}
              onClose={handleClose}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title">Pedido enviado com sucesso!</DialogTitle>
              <DialogContent>
                <DialogContentText id="alert-dialog-description">
                  Recebemos seu pedido e dentro de instantes comunicaremos via <strong>WhatsApp e E-mail</strong> todas as atualizações
                  na sua compra. Obrigado por comprar com a Max.
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button onClick={handleClose} color="primary">
                  Combinado
                </Button>
              </DialogActions>
            </Dialog>
        </div>
    )
}
export default inject('cartStore')(observer(PaymentStep));

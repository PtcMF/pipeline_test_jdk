import React, { VFC } from "react";
import {FlightLand, FlightTakeoff} from "@material-ui/icons";
import moment from "moment";
import {Flight as OfferFlight} from "../../Api/Offer"
import './style.scss';
import { Seller } from "../../Api/Cart";

export type FlightProps = OfferFlight & {direction: string, seller: Seller}

const Flight: VFC<FlightProps> = ({ direction, seller, departureAirport, departureTime, arrivalAirport, arrivalTime }) => {
    return (
        <div className='flight'>
            <div className='flight-direction'>
                {direction === "Ida" ? <FlightTakeoff /> : <FlightLand />}
                <span>
                    {direction === "Ida" ? <strong>Ida</strong> : <strong>Volta</strong>}{" - "}
                    {moment(departureTime).format('ddd DD/MM/YYYY')}
                </span>
            </div>
            <div className='flight-info'>
                <div>{seller.name}</div>
                <div>
                    <strong>{departureAirport.iataCode}</strong>
                    {moment(departureTime).format('H:mm')}
                </div>
                <div>
                    10h 50m<br/>1 parada
                </div>
                <div>
                    <strong>{arrivalAirport.iataCode}</strong>
                    {moment(arrivalTime).format('H:mm')}
                </div>
            </div>
        </div>
    )
}

export default Flight

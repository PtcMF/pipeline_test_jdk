import React, { VFC, useState, useEffect } from "react";
import {inject, observer} from "mobx-react";
import CartStore from "../Store/CartStore";
import './style.scss';
import Form from "@rjsf/material-ui";
// import passengerData from '../passenger.data.json';
import {JSONSchema7} from "json-schema";
import {CartSchema, CartApi} from "../Api/Cart";
import UiStore from "../Store/UiStore";

type TravellerStepProps = {
    cartStore?: CartStore,
    uiStore?: UiStore
}

const TravellerStep:VFC<TravellerStepProps> = ({ cartStore, uiStore }) => {
    
    const schemas = cartStore?.getSchemas() || []

    if (schemas.length === 0) {
        return null;
    }
    
    const submitSchemaData = async (schemaId: number, formData: any): Promise<void> => {
        await cartStore?.saveSchemaData(schemaId!!, formData)
        uiStore!.setStep(4);
    }


    return (
        <div className='travellers'>
            {schemas.map((schema) => {
                return <Form
                    schema={schema.definition as JSONSchema7}
                    formData={schema.data}
                    onSubmit={async (ev) => await submitSchemaData(schema.id, ev.formData)}
                />
            })}
        </div>
    )
}
export default inject('cartStore', 'uiStore')(observer(TravellerStep));

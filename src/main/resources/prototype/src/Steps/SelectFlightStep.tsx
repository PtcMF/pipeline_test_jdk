import { VFC, useState, useEffect } from "react";
import {Button} from "@material-ui/core";
import Flight from "./SelectFlightStep/Flight";
import {inject, observer} from "mobx-react";
import CartStore from "../Store/CartStore";
import {Offer, OfferApi, OfferList, Flight as OfferFlight} from "../Api/Offer";
import './style.scss';
import UiStore from "../Store/UiStore";

type SelectFlightStepProps = {
    cartStore?: CartStore,
    uiStore?: UiStore,
}

const SelectFlightStep:VFC<SelectFlightStepProps> = ({ cartStore, uiStore }) => {
    const [offers, setOffers] = useState<OfferList<OfferFlight>>([])

    useEffect(() => {
        const getOffers = async () => {
            try {
                const offerApi = new OfferApi()
                const offersAux = await offerApi.findAirfareOffers()
                setOffers(offersAux)
            } catch (err) {
                console.error(err)
            }
        }

        getOffers()
    }, []);

    const renderInboundFlight = (offer: Offer<OfferFlight>) => {
        let item: OfferFlight | null = null;
    
        // mixed rt
        if (offer.products.length === 2) {
            item = offer.products[1].itinerary[0];
        }
    
        // standard rt
        if (offer.products[0].itinerary.length === 2) {
            item = offer.products[0].itinerary[1];
        }
    
        if (null === item) {
            return null;
        }
    
        return <><hr /><Flight {...item} seller={offer.products[0].seller} direction="Volta"/></>
    }

    const selectOffer = async (offer: Offer<OfferFlight>) => {
        uiStore!.loading = true
        await cartStore!.addItem(offer.sku, 'airfare');
        uiStore!.loading = false
        uiStore!.setStep(2);
    };

    return (
        <div className='flights'>
            {offers.map((offer: Offer<OfferFlight>, index) =>
                <div className='flight-card card' key={`${offer.sku}-${index}`}>
                    <div className='flight-card-info'>
                        <Flight {...offer.products[0].itinerary[0]} seller={offer.products[0].seller} direction="Ida" />
                        {renderInboundFlight(offer)}
                    </div>
                    <div className='flight-card-select'>
                        <h5>{new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(offer.totalAmount.price / 100)}</h5>
                        <Button className="btn" onClick={() => selectOffer(offer) }>Selecionar</Button>
                    </div>
                </div>
            )}
        </div>
    )
}

export default inject('cartStore', 'uiStore')(observer(SelectFlightStep));

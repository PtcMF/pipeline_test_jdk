import React, { FunctionComponent, useState } from "react";

type StepWrapperProps = {
    visible: boolean
}

const StepContent:FunctionComponent<StepWrapperProps> = ({visible, children}) => {
    const display = visible ? 'block' : 'none';
    return <div className="step-content" style={{display}}>
        {children}
    </div>
}

export default StepContent;
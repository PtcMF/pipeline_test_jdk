import {makeObservable, observable} from 'mobx';

export default class UiStore {
    @observable
    private _currentStep: number;

    @observable
    private _loading: boolean;

    constructor() {
        makeObservable(this);
        this._currentStep = 1;
        this._loading = false;
    }

    setStep(step: number): void {
        this._currentStep = step;
    }

    get currentStep(): number {
        return this._currentStep;
    }

    get loading(): boolean {
        return this._loading;
    }

    set loading(bool: boolean) {
        this._loading = bool;
    }

    isStepActive(step: number): boolean {
        return this._currentStep === step;
    }
}

import {computed, makeObservable, observable} from 'mobx';
import {Adjustment, Cart, CartApi, CartItem, CartSchema} from "../Api/Cart";
import {CartStorage} from "../Storage/CartStorage";

export default class CartStore {
    private cartApi: CartApi;
    private storage: CartStorage;
    @observable private data?: Cart;
    @observable private schemas?: Array<CartSchema>

    constructor(cartApi: CartApi, storage: CartStorage) {
        makeObservable(this);
        this.cartApi = cartApi;
        this.storage = storage;
        this.createAndLoad();
    }

    async createAndLoad() {
        await this.create();
        await this.load();
    }

    async create() {
        if (await this.storage.has()) {
            return;
        }
        const id = await this.cartApi.create();
        await this.storage.write(id);
    }

    async load() {
        const id = await this.storage.read();

        if (undefined === id) {
            throw new Error('There is no active cart. Please create a new one before.');
        }

        try {
            const details: [Promise<Cart>, Promise<Array<CartSchema>>] = [
                this.cartApi.details(id),
                this.cartApi.getSchemas(id)
            ]

            const result = await Promise.all(details)

            this.data = result[0];
            this.schemas = result[1];
        } catch (e) {
            // await this.storage.clear();
            // await this.create();
            // this.data = await this.cartApi.details(id);
        }
    }

    async addItem(itemId: string, business: string) {
        await this.cartApi.addItem(await this.storage.read(), itemId, business);
        await this.load();
    }

    async removeItem(itemId: string) {
        await this.cartApi.removeItem(await this.storage.read(), itemId);
        await this.load();
    }

    @computed get count(): Number {
        if (!this.data) {
            return 0;
        }
        return this.data!.offers.length;
    }

    @computed get total(): Number {
        if (!this.data) {
            return 0;
        }
        return this.data!.totalAmount / 100;
    }

    @computed get subtotal(): Number {
        if (!this.data) {
            return 0;
        }
        return this.data!.baseAmount / 100;
    }

    @computed get adjustmentsTotal(): Number {
        if (!this.data) {
            return 0;
        }
        return this.data.adjustmentsAmount / 100;
    }

    @computed get items(): Array<CartItem> {
        if (!this.data) {
            return [];
        }
        return this.data.offers;
    }

    @computed get hasItems(): boolean {
        return undefined !== this.data && this.data.offers.length > 0;
    }

    @computed get adjustments(): Array<Adjustment> {
        if (!this.data) {
            return [];
        }
        return this.data.adjustments;
    }

    getSchemas(): Array<CartSchema> {
        return this.schemas!!
    }

    async saveSchemaData(schemaId: number, data: object): Promise<boolean> {
        await this.cartApi.saveSchemaData(await this.storage.read(), schemaId, data)
        await this.load()
        return true
    }

    async complete(): Promise<boolean> {
        return this.cartApi.complete(await this.storage.read())
    }
}

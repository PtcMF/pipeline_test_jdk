import React from 'react';
import {
    AppBar,
    Backdrop,
    CircularProgress,
    Step, StepButton,
    StepLabel,
    Stepper,
    Toolbar,
    Typography
} from "@material-ui/core";
import SelectFlightStep from "./Steps/SelectFlightStep";
import {inject, observer} from "mobx-react";
import CartStore from "./Store/CartStore";
import { VFC } from 'react';
import TravellerStep from "./Steps/TravellerStep";
import StepContent from "./Steps/StepContent";
import CartSummary from "./CartSummary";
import './App.scss';
import UiStore from "./Store/UiStore";
import SelectFlightHotel from './Steps/SelectHotelStep';
import SelectHotelStep from './Steps/SelectHotelStep';
import PaymentStep from './Steps/PaymentStep';

type AppProps = {
    cartStore?: CartStore,
    uiStore?: UiStore,
}

const App:VFC<AppProps> = ({ cartStore, uiStore }) => {

    let currentStep = uiStore?.currentStep || 1;

    // if (!cartStore?.hasItems) {
        // currentStep = 1;
    // }

    return (
        <>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6">
                        <img src="https://static.maxmilhas.com.br/img/logo-mm-white.svg" alt="MaxMilhas" width="114px" height="32px" />
                    </Typography>
                </Toolbar>
            </AppBar>

            <div>
                <Stepper nonLinear activeStep={currentStep - 1} orientation="horizontal">
                    <Step key="select-flight">
                        <StepButton onClick={() => uiStore?.setStep(1)}>
                            Selecionar vôo
                        </StepButton>
                    </Step>
                    <Step key="select-hotel">
                        <StepButton onClick={() => uiStore?.setStep(2)}>
                            Selecionar hotel
                        </StepButton>
                    </Step>
                    <Step key="travellers">
                        <StepButton onClick={() => uiStore?.setStep(3)}>
                            Viajantes
                        </StepButton>
                    </Step>
                    <Step key="checkout">
                        <StepButton onClick={() => uiStore?.setStep(4)}>
                            Pagamento
                        </StepButton>
                    </Step>
                </Stepper>
            </div>

            <div className='container'>
                <div className='flight-container'>
                    <StepContent visible={currentStep === 1}>
                        <SelectFlightStep />
                    </StepContent>
                    <StepContent visible={currentStep === 2}>
                        <SelectHotelStep />
                    </StepContent>
                    <StepContent visible={currentStep === 3}>
                        <TravellerStep />
                    </StepContent>
                    <StepContent visible={currentStep === 4}>
                        <PaymentStep />
                    </StepContent>
                </div>
                <CartSummary />
            </div>
            <Backdrop style={{zIndex: 999}} open={uiStore!.loading}>
                <CircularProgress color="inherit" />
            </Backdrop> 
        </>
    );
}

export default inject('cartStore', 'uiStore')(observer(App));

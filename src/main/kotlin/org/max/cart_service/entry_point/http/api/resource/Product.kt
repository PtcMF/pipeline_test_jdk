package org.max.cart_service.entry_point.http.api.resource

import io.swagger.v3.oas.annotations.media.Schema

data class Product(

    @Schema(example = "Product")
    val type: String,

    @Schema(example = "Accommodation")
    val additionalType: String,

    val seller: Map<*, *>,

    @Schema(example = "1 quarto, 1 hóspede, 7 noites")
    val description: String,

    @Schema(example = "50050")
    val baseAmount: Long,

    @Schema(example = "10055")
    val adjustmentAmount: Long,

    @Schema(example = "60105")
    val totalAmount: Long,

    val adjustments: List<CartAdjustment> = emptyList(),
)

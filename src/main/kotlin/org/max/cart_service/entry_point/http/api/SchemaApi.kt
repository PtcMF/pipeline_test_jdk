package org.max.cart_service.entry_point.http.api

import com.fasterxml.jackson.databind.ObjectMapper
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import io.swagger.v3.oas.annotations.tags.Tag
import org.json.JSONObject
import com.fasterxml.jackson.module.kotlin.readValue
import org.max.cart_service.application.command.SaveSchemaData
import org.max.cart_service.domain.entity.Cart
import org.max.cart_service.domain.exception.CartNotFoundException
import org.max.cart_service.domain.repository.CartRepository
import org.max.cart_service.entry_point.http.api.error_handler.ErrorResponse
import org.max.cart_service.entry_point.http.api.factory.CartFactory
import org.max.cart_service.entry_point.http.api.resource.CartSchema
import org.max.messenger.core.MessageBus
import org.max.messenger.core.stamp.HandledStamp
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.max.cart_service.entry_point.http.api.resource.Cart as CartResource

@RestController
@Tag(name="Cart schema")
class SchemaApi @Autowired constructor(
    private val carts: CartRepository,
    private val cartFactory: CartFactory,
    private val commandBus: MessageBus
) {
    @GetMapping("/v1/carts/{number}/schemas", produces = ["application/json"])
    @Operation(summary = "Retrieve the schemas available in the cart")
    @ApiResponses(
        ApiResponse(responseCode = "404", content = [Content(schema = Schema(implementation = ErrorResponse::class))]),
        ApiResponse(responseCode = "200"),
    )
    fun listSchemas(@PathVariable number: String): ResponseEntity<List<CartSchema>> {

        val cart = carts.findOneByNumber(number) ?: throw CartNotFoundException(number)
        val objectMapper = ObjectMapper()

        val schemas = mutableListOf<CartSchema>()
        cart.offers.forEach {
            it.schemas.forEach { cartOfferSchema ->
                schemas += CartSchema(
                    id = cartOfferSchema.id!!,
                    name = cartOfferSchema.name,
                    definition = objectMapper.readValue(cartOfferSchema.definition),
                    data = if (cartOfferSchema.data != null) objectMapper.readValue(cartOfferSchema.data!!) else null
                )
            }
        }

        return ResponseEntity<List<CartSchema>>(schemas, HttpStatus.CREATED)
    }

    @PutMapping("/v1/carts/{number}/schemas/{schemaId}", produces = ["application/json"])
    @Operation(summary = "Save a data available in the cart")
    @ApiResponses(
        ApiResponse(responseCode = "404", content = [Content(schema = Schema(implementation = ErrorResponse::class))]),
        ApiResponse(responseCode = "200"),
    )
    fun saveSchema(@PathVariable number: String, @PathVariable schemaId: Long, @RequestBody data: Map<String, Any>): ResponseEntity<CartResource> {

        val content = ObjectMapper().writeValueAsString(data)

        val command = SaveSchemaData(
            cartNumber = number,
            schemaId = schemaId,
            data = content
        )

        val envelope = commandBus.dispatch(command)
        val cart = envelope.lastOf<HandledStamp>()?.result as Cart

        return ResponseEntity<CartResource>(cartFactory.create(cart), HttpStatus.CREATED)
    }
}
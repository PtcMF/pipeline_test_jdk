package org.max.cart_service.entry_point.http.api

import org.max.cart_service.application.command.AddCartOffer
import org.max.cart_service.application.command.RemoveCartOffer
import org.max.cart_service.domain.repository.CartRepository
import org.max.cart_service.entry_point.http.api.error_handler.ErrorResponse
import org.max.cart_service.entry_point.http.api.factory.CartFactory
import org.max.cart_service.entry_point.http.api.resource.Cart
import org.max.cart_service.entry_point.http.api.resource.CartOffer
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import io.swagger.v3.oas.annotations.tags.Tag
import org.max.messenger.core.MessageBus
import org.max.messenger.core.stamp.HandledStamp
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.context.request.WebRequest
import org.max.cart_service.domain.entity.Cart as CartEntity

@RestController
@Tag(name="Cart offer")
class CartOfferApi @Autowired constructor(
    private val carts: CartRepository,
    private val cartFactory: CartFactory,
    private val commandBus: MessageBus
) {

    @PostMapping("/v1/carts/{number}/offers", consumes = ["application/json"],  produces = ["application/json"])
    @Operation(summary = "Adds an offer to the cart")
    @ApiResponses(
        ApiResponse(responseCode = "400", content = [Content(schema = Schema(implementation = ErrorResponse::class))]),
        ApiResponse(responseCode = "404", content = [Content(schema = Schema(implementation = ErrorResponse::class))]),
        ApiResponse(responseCode = "201"),
    )
    fun add(
        @PathVariable number: String,
        @RequestBody offer: CartOffer,
        request: WebRequest
    ): ResponseEntity<Cart> {
        val params = mutableMapOf<String, String>()
        request.parameterNames.forEach { params[it] = request.parameterMap[it]!!.first() }
        val command = AddCartOffer(number, offer.sku, params)
        val envelope = commandBus.dispatch(command)
        val cart = envelope.lastOf<HandledStamp>()?.result as CartEntity
        return ResponseEntity<Cart>(cartFactory.create(cart), HttpStatus.OK)
    }

    @DeleteMapping("/v1/carts/{number}/offers/{sku}", produces = ["application/json"])
    @Operation(summary = "Remove an offer from the cart")
    @ApiResponses(
        ApiResponse(responseCode = "404", content = [Content(schema = Schema(implementation = ErrorResponse::class))]),
        ApiResponse(responseCode = "200"),
    )
    fun remove(@PathVariable number: String, @PathVariable sku: String): ResponseEntity<Cart> {
        val envelope = commandBus.dispatch(RemoveCartOffer(number, sku))
        val cart = envelope.lastOf<HandledStamp>()?.result as CartEntity
        return ResponseEntity<Cart>(cartFactory.create(cart), HttpStatus.OK)
    }
}

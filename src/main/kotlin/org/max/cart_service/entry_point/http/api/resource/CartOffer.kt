package org.max.cart_service.entry_point.http.api.resource

import io.swagger.v3.oas.annotations.media.Schema

data class CartOffer(
    @Schema(example = "9dbf96c7-edaa-4d89-b9c8-ee9901e74ea4")
    val sku: String,

    @Schema(example = "50050")
    val baseAmount: Long,

    @Schema(example = "10055")
    val adjustmentAmount: Long,

    val products: List<Product>?,

    @Schema(example = "60105")
    val totalAmount: Long,

    val availabilityStarts: String?,
    val availabilityEnds: String?
)

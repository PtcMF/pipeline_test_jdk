package org.max.cart_service.entry_point.http.api.factory

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.max.cart_service.entry_point.http.api.resource.CartOffer
import org.max.cart_service.entry_point.http.api.resource.Product
import org.springframework.stereotype.Component
import java.time.format.DateTimeFormatter
import org.max.cart_service.domain.entity.CartOffer as CartOfferEntity

@Component
class CartOfferFactory(
    private val monetaryAmountFactory: MonetaryAmountFactory,
    private val adjustmentFactory: CartAdjustmentFactory,
    private val objectMapper: ObjectMapper
)
{
    fun create(offers: List<CartOfferEntity>): List<CartOffer> = offers.map { create(it) }

    fun create(offer: CartOfferEntity): CartOffer {
        val products = objectMapper.readValue<List<Map<String, Any>>>(offer.products).map { createProduct(it) }
        return CartOffer(
            sku = offer.sku,
            products = products,
            baseAmount = monetaryAmountFactory.create(offer.baseAmount),
            adjustmentAmount = monetaryAmountFactory.create(offer.adjustmentAmount),
            totalAmount = monetaryAmountFactory.create(offer.totalAmount),
            availabilityStarts = offer.availabilityStarts?.format(DateTimeFormatter.ISO_DATE_TIME),
            availabilityEnds = offer.availabilityEnds?.format(DateTimeFormatter.ISO_DATE_TIME)
        )
    }

    private fun createProduct(data: Map<String, Any>): Product = Product(
        type = data["type"].toString(),
        additionalType = data["additionalType"].toString(),
        seller = data["seller"] as Map<*, *>,
        description = data["description"].toString(),
        baseAmount = ((data["baseAmount"] as Map<*, *>)["price"] as Int).toLong(),
        adjustmentAmount = ((data["adjustmentsAmount"] as Map<*, *>)["price"] as Int).toLong(),
        totalAmount = ((data["totalAmount"] as Map<*, *>)["price"] as Int).toLong(),
        adjustments = (data["adjustments"] as List<Map<String, Any>>).map { adjustmentFactory.create(it) }
    )
}
package org.max.cart_service.entry_point.http.api.resource

import io.swagger.v3.oas.annotations.media.Schema

data class CartAdjustment (
    @Schema(example = "FEE_SERVICE")
    val code: String,

    @Schema(example = "50.5")
    val amount: Long
)

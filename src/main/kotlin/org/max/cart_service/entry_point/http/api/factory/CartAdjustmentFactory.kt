package org.max.cart_service.entry_point.http.api.factory

import org.javamoney.moneta.Money
import org.max.cart_service.entry_point.http.api.resource.CartAdjustment
import org.springframework.stereotype.Component
import org.max.cart_service.domain.entity.CartAdjustment as CartAdjustmentEntity

@Component
class CartAdjustmentFactory (private val monetaryAmountFactory: MonetaryAmountFactory) {

    fun create(adjustments: List<CartAdjustmentEntity>): List<CartAdjustment> {
        val grouped = mutableListOf<CartAdjustment>()

        // group adjustments by code and sum its amount
        adjustments.groupBy { it.code }.forEach { group ->
            var amount = Money.of(0, group.value.first().amount.currency)
            group.value.forEach { adj ->
                amount = amount.add(adj.amount)
            }
            grouped += CartAdjustment(
                code = group.key,
                amount = monetaryAmountFactory.create(amount)
            )
        }

        return grouped
    }

    fun create(adjustment: CartAdjustmentEntity): CartAdjustment = CartAdjustment(
        code = adjustment.code,
        amount = monetaryAmountFactory.create(adjustment.amount)
    )

    fun create(adjustment: Map<String, Any>): CartAdjustment = CartAdjustment(
        code = adjustment["code"].toString(),
        amount = ((adjustment["amount"] as Map<*, *>)["price"] as Int).toLong()
    )
}
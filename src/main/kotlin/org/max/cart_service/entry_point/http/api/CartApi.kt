package org.max.cart_service.entry_point.http.api

import org.max.cart_service.application.command.CreateCart
import org.max.cart_service.domain.exception.CartNotFoundException
import org.max.cart_service.domain.repository.CartRepository
import org.max.cart_service.entry_point.http.api.error_handler.ErrorResponse
import org.max.cart_service.entry_point.http.api.factory.CartFactory
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import io.swagger.v3.oas.annotations.tags.Tag
import org.max.cart_service.application.command.PlaceOrder
import org.max.messenger.core.MessageBus
import org.max.messenger.core.stamp.HandledStamp
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.max.cart_service.application.model.CartOffer as CartOfferModel
import org.max.cart_service.domain.entity.Cart as CartEntity
import org.max.cart_service.entry_point.http.api.resource.Cart as CartResource
import org.max.cart_service.entry_point.http.api.resource.CartOffer as CartOfferResource

@RestController
@Tag(name="Cart")
class CartApi @Autowired constructor(
    private val carts: CartRepository,
    private val cartFactory: CartFactory,
    private val commandBus: MessageBus
) {

    @GetMapping("/v1/carts/{number}", produces = ["application/json"])
    @Operation(summary = "Retrieve the given cart by its number")
    @ApiResponses(
        ApiResponse(responseCode = "404", content = [Content(schema = Schema(implementation = ErrorResponse::class))]),
        ApiResponse(responseCode = "200"),
    )
    fun get(@PathVariable number: String): ResponseEntity<CartResource> {
        val cart = carts.findOneByNumber(number) ?: throw CartNotFoundException(number)
        return ResponseEntity<CartResource>(cartFactory.create(cart), HttpStatus.OK)
    }

    @PostMapping("/v1/carts", consumes = ["application/json"], produces = ["application/json"])
    @Operation(summary = "Create a new cart")
    @ApiResponses(
        ApiResponse(responseCode = "400", content = [Content(schema = Schema(implementation = ErrorResponse::class))]),
        ApiResponse(responseCode = "404", description = "Offer Not Found", content = [Content(schema = Schema(implementation = ErrorResponse::class))]),
        ApiResponse(responseCode = "201"),
    )
    fun create(@RequestBody(required = false) offers: List<CartOfferResource>?): ResponseEntity<CartResource> {

        val list = offers ?: emptyList()
        val command = CreateCart(offers = list.map { CartOfferModel(it.sku) })
        val envelope = commandBus.dispatch(command)
        val cart = envelope.lastOf<HandledStamp>()?.result as CartEntity

        return ResponseEntity(cartFactory.create(cart), HttpStatus.CREATED)
    }

    @PutMapping("/v1/carts/{number}", consumes = ["application/json"],  produces = ["application/json"])
    @Operation(summary = "Completes the order")
    @ApiResponses(
        ApiResponse(responseCode = "400", content = [Content(schema = Schema(implementation = ErrorResponse::class))]),
        ApiResponse(responseCode = "404", content = [Content(schema = Schema(implementation = ErrorResponse::class))]),
        ApiResponse(responseCode = "202"),
    )
    fun complete(@PathVariable number: String): ResponseEntity<CartResource> {
        val command = PlaceOrder(number)
        val envelope = commandBus.dispatch(command)
        val cart = envelope.lastOf<HandledStamp>()?.result as CartEntity
        return ResponseEntity<CartResource>(cartFactory.create(cart), HttpStatus.ACCEPTED)
    }
}

package org.max.cart_service.entry_point.http.api.resource

import io.swagger.v3.oas.annotations.media.Schema
import org.json.JSONObject

data class CartSchema (
    @Schema(example = "100")
    val id: Long,

    @Schema(example = "PassengerList")
    val name: String,

    @Schema(example = """{"type":"array"}""", description = "Encoded JSON schema.")
    val definition: Map<String, Any>,

    @Schema(example = """{"passengers":[{"firstName":"John"}]}""", description = "Data related to the schema definition")
    val data: Map<String, Any>? = null
)
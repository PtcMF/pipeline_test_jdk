package org.max.cart_service.entry_point.http.api

import org.max.cart_service.domain.repository.CartRepository
import org.max.cart_service.entry_point.http.api.factory.CartFactory
import org.max.cart_service.entry_point.http.api.resource.Cart
import org.max.cart_service.entry_point.http.api.resource.Invoice
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@Tag(name="Checkout")
class InvoiceApi @Autowired constructor(
    private val carts: CartRepository,
    private val cartFactory: CartFactory
) {

    @GetMapping("/v1/carts/{number}/invoices", produces = ["application/json"])
    @Operation(summary = "Return the invoice data of the cart")
    fun get(@PathVariable number: String): ResponseEntity<Invoice> {
        return ResponseEntity<Invoice>(Invoice("Tales"), HttpStatus.OK)
    }

    @PostMapping("/v1/carts/{number}/invoices", consumes = ["application/json"], produces = ["application/json"])
    @Operation(summary = "Sets the invoice of the cart")
    fun create(@PathVariable number: String, @RequestBody invoice: Invoice): ResponseEntity<Cart> {
        val cart = carts.findOneByNumber(number)
        return ResponseEntity<Cart>(cartFactory.create(cart!!), HttpStatus.OK)
    }
}
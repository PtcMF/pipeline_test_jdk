package org.max.cart_service.entry_point.http.api.error_handler

import org.max.cart_service.domain.exception.ResourceNotFound
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class ExceptionHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler(ResourceNotFound::class)
    fun handle(ex: ResourceNotFound): ResponseEntity<ErrorResponse> {
        val body = ErrorResponse(404, ex::class.simpleName!!, ex.message!!, emptyMap())
        return ResponseEntity<ErrorResponse>(body, HttpStatus.NOT_FOUND)
    }
}
package org.max.cart_service.entry_point.http.api.factory

import org.springframework.stereotype.Component
import javax.money.MonetaryAmount

@Component
class MonetaryAmountFactory {
    fun create(amount: MonetaryAmount): Long {
        return amount.number.toInt().toLong()
    }
}
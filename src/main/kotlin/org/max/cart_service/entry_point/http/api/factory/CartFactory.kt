package org.max.cart_service.entry_point.http.api.factory

import org.max.cart_service.entry_point.http.api.resource.Cart
import org.max.cart_service.domain.entity.Cart as CartEntity
import org.springframework.stereotype.Component
import java.time.format.DateTimeFormatter

@Component
class CartFactory (
    private val offerFactory: CartOfferFactory,
    private val adjustmentFactory: CartAdjustmentFactory,
    private val monetaryAmount: MonetaryAmountFactory
) {
    fun create(cart: CartEntity): Cart {
        return Cart(
            number = cart.number,
            currency = cart.currency,
            baseAmount = monetaryAmount.create(cart.baseAmount),
            adjustmentAmount = monetaryAmount.create(cart.adjustmentAmount),
            totalAmount = monetaryAmount.create(cart.totalAmount),
            offers = offerFactory.create(cart.offers),
            adjustments = adjustmentFactory.create(cart.adjustments),
            expiresAt = cart.expiresAt?.format(DateTimeFormatter.ISO_DATE_TIME)
        )
    }
}
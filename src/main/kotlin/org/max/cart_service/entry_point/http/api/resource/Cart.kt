package org.max.cart_service.entry_point.http.api.resource

import io.swagger.v3.oas.annotations.media.Schema

data class Cart(
    @Schema(example = "XPTO-123", description = "Unique cart number")
    val number: String,

    @Schema(example = "BRL")
    val currency: String?,

    @Schema(example = "50050")
    val baseAmount: Long,

    @Schema(example = "10055")
    val adjustmentAmount: Long,

    @Schema(example = "60105")
    val totalAmount: Long,

    val offers: List<CartOffer> = emptyList(),
    val adjustments: List<CartAdjustment> = emptyList(),
    val expiresAt: String?
)

package org.max.cart_service.infrastructure.client

import org.max.cart_service.application.client.OfferClient
import org.max.cart_service.application.model.CartAdjustment
import org.max.cart_service.application.model.CartOffer
import org.max.cart_service.application.model.CartOfferSchema
import org.max.cart_service.domain.exception.CartOfferNotFoundException
import org.javamoney.moneta.Money
import org.json.JSONArray
import org.json.JSONObject
import org.springframework.stereotype.Component
import java.time.LocalDateTime
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import javax.money.MonetaryAmount

@Component
class HttpOfferClient : OfferClient {

    override fun find(sku: String, context: Map<String, String>): CartOffer {
        val response = khttp.get(
            url = "http://localhost:8080/offers/$sku",
            params = context,
            timeout = 120.0,
        )

        if (response.statusCode == 404) {
            throw CartOfferNotFoundException(sku)
        }

        if (response.statusCode == 500) {
            throw IllegalArgumentException(
                "Impossible to get offer details: \"${response.jsonObject["message"]}\""
            )
        }

        val json = response.jsonObject

        return CartOffer(
            sku = sku,
            products = json["products"].toString(),
            schemas = readSchemaList(json["schemas"] as JSONArray),
            baseAmount = createMonetaryAmount(json["baseAmount"] as JSONObject),
            adjustmentAmount = createMonetaryAmount(json["adjustmentsAmount"] as JSONObject),
            totalAmount = createMonetaryAmount(json["totalAmount"] as JSONObject),
            adjustments = readAdjustmentList(json["adjustments"] as JSONArray),
            availabilityStarts = LocalDateTime.parse(json.getString("availabilityStarts"), DateTimeFormatter.ISO_DATE_TIME),
            availabilityEnds = LocalDateTime.parse(json.getString("availabilityEnds"), DateTimeFormatter.ISO_DATE_TIME)
        )
    }

    private fun readSchemaList(jsonSchemas: JSONArray): List<CartOfferSchema> {

        val schemas = mutableListOf<CartOfferSchema>()

        for (schema in jsonSchemas) {
            schemas += CartOfferSchema(
                name = (schema as JSONObject)["name"].toString(),
                definition = schema["definition"].toString(), // todo mudar para "definition" no mock
            )
        }

        return schemas
    }

    private fun readAdjustmentList(jsonAdjustments: JSONArray): List<CartAdjustment> {
        val adjustments = mutableListOf<CartAdjustment>()

        for (adjustment in jsonAdjustments) {
            adjustments += CartAdjustment(
                code = (adjustment as JSONObject)["code"].toString(),
                amount = createMonetaryAmount(adjustment["amount"] as JSONObject),
            )
        }

        return adjustments
    }

    private fun createMonetaryAmount(value: JSONObject): MonetaryAmount {
        return Money.of(value["price"] as Number, value["currency"] as String)
    }
}

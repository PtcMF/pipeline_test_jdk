package org.max.cart_service.infrastructure.application

import org.max.cart_service.application.guard.CartGuard
import org.max.cart_service.application.guard.CompositeGuard
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class CartGuardConfiguration {

    @Bean
    fun cartGuard(context: ApplicationContext): CartGuard {
        val childrenGuards = context.getBeansOfType(CartGuard::class.java)
        return CompositeGuard(childrenGuards.entries.mapTo(mutableListOf()) { it.value })
    }
}

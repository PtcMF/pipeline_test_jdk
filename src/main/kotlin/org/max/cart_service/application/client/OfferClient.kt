package org.max.cart_service.application.client

import org.max.cart_service.application.model.CartOffer

/**
 * Client to interact with offer service
 */
interface OfferClient {

    /**
     * Find an offer by its SKU
     */
    fun find(sku: String, context: Map<String, String> = emptyMap()): CartOffer
}
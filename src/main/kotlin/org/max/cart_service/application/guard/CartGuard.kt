package org.max.cart_service.application.guard

import org.max.cart_service.domain.entity.Cart

/**
 * Cart guards are services aimed to check the entire cart's state
 * before send the cart to the next step on order pipeline. Each
 * guard should throw an exception in case of failure on validation
 * process.
 */
interface CartGuard {
    fun validate(cart: Cart)
}
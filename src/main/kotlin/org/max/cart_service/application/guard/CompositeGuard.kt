package org.max.cart_service.application.guard

import org.max.cart_service.domain.entity.Cart

class CompositeGuard (private val guards: List<CartGuard>) : CartGuard {
    override fun validate(cart: Cart) {
        guards.forEach { it.validate(cart) }
    }

    fun hasGuard(): Boolean {
        return guards.isNotEmpty()
    }
}
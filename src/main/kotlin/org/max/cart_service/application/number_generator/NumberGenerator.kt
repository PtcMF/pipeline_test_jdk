package org.max.cart_service.application.number_generator

import org.springframework.stereotype.Component
import java.util.*

/**
 * Generates a random number to be referenced to the cart. This number
 * will be exposed for future reference by the customers and operators
 */
interface NumberGenerator {
    fun generate(): String;
}

@Component
class DefaultNumberGenerator : NumberGenerator {
    override fun generate(): String {
        return UUID.randomUUID().toString()
    }
}
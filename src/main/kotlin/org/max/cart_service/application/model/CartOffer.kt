package org.max.cart_service.application.model

import java.time.LocalDateTime
import javax.money.MonetaryAmount

data class CartOffer(
    val sku: String,
    val baseAmount: MonetaryAmount? = null,
    val adjustmentAmount: MonetaryAmount? = null,
    val totalAmount: MonetaryAmount? = null,
    val adjustments: List<CartAdjustment> = emptyList(),
    val products: String? = null,
    val schemas: List<CartOfferSchema> = emptyList(),
    val availabilityStarts: LocalDateTime? = null,
    val availabilityEnds: LocalDateTime? = null
)

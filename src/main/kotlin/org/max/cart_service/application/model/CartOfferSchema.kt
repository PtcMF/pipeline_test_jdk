package org.max.cart_service.application.model

data class CartOfferSchema(
    val name: String,
    val definition: String
)
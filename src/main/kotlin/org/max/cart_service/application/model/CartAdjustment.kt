package org.max.cart_service.application.model

import javax.money.MonetaryAmount

data class CartAdjustment (
    val code: String,
    val amount: MonetaryAmount
)
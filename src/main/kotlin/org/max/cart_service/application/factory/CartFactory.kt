package org.max.cart_service.application.factory

import org.max.cart_service.application.client.OfferClient
import org.max.cart_service.application.command.CreateCart
import org.max.cart_service.application.number_generator.NumberGenerator
import org.max.cart_service.domain.entity.Cart
import org.max.cart_service.domain.entity.CartAdjustment
import org.max.cart_service.domain.entity.CartOfferSchema
import org.springframework.stereotype.Component

@Component("applicationCartService")
class CartFactory(
    private val numberGenerator: NumberGenerator,
    private val offerClient: OfferClient
) {

    fun create(command: CreateCart): Cart {
        val cart = Cart(number = numberGenerator.generate())

        command.offers.forEach {

            val offerModel = offerClient.find(it.sku)

            cart.add(
                sku = offerModel.sku,
                baseAmount = offerModel.baseAmount!!,
                adjustmentAmount = offerModel.adjustmentAmount!!,
                totalAmount = offerModel.totalAmount!!,
                adjustments = offerModel.adjustments.map { adjustmentModel ->
                    CartAdjustment(
                        code = adjustmentModel.code,
                        amount = adjustmentModel.amount
                    )
                },
                schemas = offerModel.schemas.map { schemaModel ->
                    CartOfferSchema(
                        name = schemaModel.name,
                        definition = schemaModel.definition
                    )
                },
                products = offerModel.products!!,
                availabilityStarts = offerModel.availabilityStarts,
                availabilityEnds = offerModel.availabilityEnds
            )
        }

        return cart
    }
}

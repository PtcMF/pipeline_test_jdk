package org.max.cart_service.application.command

data class AddCartOffer (
    val cartNumber: String,
    val sku: String,
    val context: Map<String, String> = emptyMap()
)
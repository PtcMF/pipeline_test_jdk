package org.max.cart_service.application.command

data class RemoveCartOffer (val cartNumber: String, val sku: String)
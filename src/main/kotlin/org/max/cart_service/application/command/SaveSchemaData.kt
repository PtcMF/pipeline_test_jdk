package org.max.cart_service.application.command

data class SaveSchemaData(val cartNumber: String, val schemaId: Long, val data: String)

package org.max.cart_service.application.command

data class PlaceOrder(
    val cartNumber: String,
)

package org.max.cart_service.application.command

import org.max.cart_service.application.model.CartOffer

data class CreateCart (
    val offers: List<CartOffer> = emptyList()
)

package org.max.cart_service.application.event_handler

import org.json.JSONArray
import org.json.JSONObject
import org.max.cart_service.domain.event.OrderWasPlaced
import org.max.cart_service.domain.exception.CartNotFoundException
import org.max.cart_service.domain.repository.CartRepository
import org.max.messenger.core.Envelope
import org.max.messenger.core.annotation.Handler
import org.springframework.stereotype.Component
import java.util.*

/**
 * Classe para simular fechamento de hotel.
 *
 * TODO remover esta classe quando tivermos o fechamento de hotel
 * sendo realizado pelo OrderService.
 */
@Component
class PlaceLodgingOrderHandler (private val carts: CartRepository) {

    @Handler
    fun handle(envelope: Envelope<OrderWasPlaced>) {
        val cart = carts.findOneByNumber(envelope.message.cartNumber) ?: throw CartNotFoundException(envelope.message.cartNumber)
        cart.offers.forEach {
            val products = JSONArray(it.products)

            products.forEach { product ->
                val type = (product as JSONObject)["type"].toString()
                if (type == "@Accommodation") {
                    book(product)
                }
            }
        }
    }

    private fun book(product: JSONObject): UUID {
        val rateToken = product["rateToken"] as String
        val roomId = product["id"] as String
        val hotelId = (product["seller"] as JSONObject)["id"]
        val payload = """
        {
            "book": {
                "hotel_id": "$hotelId",
                "rooms": [
                    {
                        "room_id": "$roomId",
                        "breakfast": "undefined",
                        "ratePlanId": "plan0",
                        "adults": 1,
                        "kids": 1,
                        "kidsAges": [
                            0
                        ],
                        "refundable": true,
                        "adult_names": [
                            "JOAO DA SILVA",
                            "MARIA DA SILVA"
                        ],
                        "kids_names": [
                            "MANUEL DA SILVA"
                        ],
                        "rateKey": "$rateToken",
                        "paymentType": "AT_WEB",
                        "rateComments": null
                    }
                ],
                "start_iso_date": "2021-07-01",
                "end_iso_date": "2021-07-02",
                "app": false,
                "lang": "PT",
                "currency": "BRL"
            },
            "card": {
                "holder_name": "JOAO DA SILVA",
                "card_number": "5555 5555 5555 5557",
                "code": "931",
                "month": "10",
                "year": "2028",
                "cpf": "37517036084",
                "rg": "1554131",
                "nascimento": "15/01/1981",
                "sexo": "M",
                "endereco": {
                    "Logradouro": "Rua Casas Novas",
                    "Complemento": "",
                    "Numero": "123",
                    "Bairro": "Neopolis ",
                    "Cidade": "Natal ",
                    "UF": "RN",
                    "CEP": "59.123-132",
                    "Pais": "BR"
                },
                "telefone": {
                    "Tipo": "6",
                    "Numero": "999999999",
                    "DDD": "84",
                    "DDI": "55"
                },
                "same_user": true,
                "sessionId": "aaee6f9c-9660-4d51-bfef-8d3832221bcb",
                "email": "undefined",
                "fone": "undefined"
            },
            "installments": 1,
            "coupon": null,
            "mercadoPagoDetails": null,
            "origin": 2,
            "mercadoPagoPayment": null
        }"""
        val json = JSONObject(payload)

        val response = khttp.post(
            url = "https://homolog2.lancehoteis.com/api/booking",
            headers = mapOf("x-device-id" to "b01765bc-2ce8-4d5f-a940-8e79ce15d479"),
            json = json
        )

        return UUID.fromString(response.text)
    }
}

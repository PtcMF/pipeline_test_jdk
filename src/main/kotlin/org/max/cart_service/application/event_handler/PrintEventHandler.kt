package org.max.cart_service.application.event_handler

import org.max.cart_service.domain.event.CartOfferWasAdded
import org.max.cart_service.domain.event.CartOfferWasRemoved
import org.max.cart_service.domain.event.CartWasCreated
import org.max.messenger.core.Envelope
import org.max.messenger.core.annotation.Handler
import org.springframework.stereotype.Component

@Component
class PrintEventHandler {

    @Handler("eventBus")
    fun handle(envelope: Envelope<CartWasCreated>) {
        println(envelope.message)
    }

//    @Handler("eventBus")
//    fun handle(envelope: Envelope<CartOfferWasRemoved>) {
//        println(envelope.message)
//    }
//
//    @Handler("eventBus")
//    fun handle(envelope: Envelope<CartOfferWasAdded>) {
//        println(envelope.message)
//    }
}
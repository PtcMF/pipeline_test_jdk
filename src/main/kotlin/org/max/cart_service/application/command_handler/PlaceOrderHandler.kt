package org.max.cart_service.application.command_handler

import org.max.cart_service.application.command.PlaceOrder
import org.max.cart_service.application.event_handler.PlaceLodgingOrderHandler
import org.max.cart_service.application.guard.CartGuard
import org.max.cart_service.domain.entity.Cart
import org.max.cart_service.domain.event.OrderWasPlaced
import org.max.cart_service.domain.exception.CartNotFoundException
import org.max.cart_service.domain.repository.CartRepository
import org.max.messenger.core.Envelope
import org.max.messenger.core.MessageBus
import org.max.messenger.core.annotation.Handler
import org.springframework.stereotype.Component

@Component
class PlaceOrderHandler(
    val carts: CartRepository,
    val eventBus: MessageBus,
    val guard: CartGuard,
) {
    @Handler("commandBus")
    fun handle(envelope: Envelope<PlaceOrder>): Cart {

        val cart = carts.findOneByNumber(envelope.message.cartNumber) ?: throw CartNotFoundException(envelope.message.cartNumber)

        guard.validate(cart)

        val event = OrderWasPlaced(envelope.message.cartNumber)

        try {
            eventBus.dispatch(event)
        } catch (ex: Throwable) {
            // todo log error here
            println(ex)
        }

        return cart
    }
}
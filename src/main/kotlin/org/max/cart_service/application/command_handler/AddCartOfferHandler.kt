import org.max.cart_service.application.client.OfferClient
import org.max.cart_service.application.command.AddCartOffer
import org.max.cart_service.domain.entity.Cart
import org.max.cart_service.domain.entity.CartAdjustment
import org.max.cart_service.domain.entity.CartOfferSchema
import org.max.cart_service.domain.event.CartOfferWasAdded
import org.max.cart_service.domain.exception.CartNotFoundException
import org.max.cart_service.domain.repository.CartRepository
import org.max.messenger.core.Envelope
import org.max.messenger.core.MessageBus
import org.max.messenger.core.annotation.Handler
import org.springframework.stereotype.Component
import java.math.BigDecimal


@Component
class AddCartOfferHandler(
    val carts: CartRepository,
    val offerClient: OfferClient,
    val eventBus: MessageBus
) {
    @Handler("envelope.messageBus")
    fun handle(envelope: Envelope<AddCartOffer>): Cart {
        val cart = carts.findOneByNumber(envelope.message.cartNumber) ?: throw CartNotFoundException(envelope.message.cartNumber)
        val offerModel = offerClient.find(envelope.message.sku, envelope.message.context)

        cart.add(
            sku = offerModel.sku,
            baseAmount = offerModel.baseAmount!!,
            adjustmentAmount = offerModel.adjustmentAmount!!,
            totalAmount = offerModel.totalAmount!!,
            adjustments = offerModel.adjustments.map { adjustmentModel ->
                CartAdjustment(
                    code = adjustmentModel.code,
                    amount = adjustmentModel.amount
                )
            },
            schemas = offerModel.schemas.map { schemaModel ->
                CartOfferSchema(
                    name = schemaModel.name,
                    definition = schemaModel.definition
                )
            },
            products = offerModel.products!!,
            availabilityStarts = offerModel.availabilityStarts,
            availabilityEnds = offerModel.availabilityEnds
        )

        this.carts.save(cart)

        try {
            eventBus.dispatch(CartOfferWasAdded(cart.number, cart.find(envelope.message.sku)))
        } catch (ex: Throwable) {
            // todo log error here
            println(ex)
        }

        return cart
    }
}
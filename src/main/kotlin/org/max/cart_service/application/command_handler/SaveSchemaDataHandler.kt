package org.max.cart_service.application.command_handler

import org.max.cart_service.application.command.SaveSchemaData
import org.max.cart_service.domain.entity.Cart
import org.max.cart_service.domain.exception.CartNotFoundException
import org.max.cart_service.domain.repository.CartRepository
import org.max.messenger.core.Envelope
import org.max.messenger.core.annotation.Handler
import org.springframework.stereotype.Component

@Component
class SaveSchemaDataHandler (val carts: CartRepository) {

    @Handler
    fun handle(envelop: Envelope<SaveSchemaData>): Cart {

        val cart = carts.findOneByNumber(envelop.message.cartNumber) ?: throw CartNotFoundException(envelop.message.cartNumber)

        // todo validate schema data through offer content service

        cart.setSchemaData(envelop.message.schemaId, envelop.message.data)
        carts.save(cart)

        return cart
    }
}
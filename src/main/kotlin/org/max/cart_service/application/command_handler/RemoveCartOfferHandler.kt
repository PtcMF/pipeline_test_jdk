package org.max.cart_service.application.command_handler

import org.max.cart_service.application.command.RemoveCartOffer
import org.max.cart_service.domain.entity.Cart
import org.max.cart_service.domain.event.CartOfferWasRemoved
import org.max.cart_service.domain.exception.CartNotFoundException
import org.max.cart_service.domain.repository.CartRepository
import org.max.messenger.core.Envelope
import org.max.messenger.core.MessageBus
import org.max.messenger.core.annotation.Handler
import org.springframework.stereotype.Component

@Component
class RemoveCartOfferHandler(
    val carts: CartRepository,
    val eventBus: MessageBus
) {
    @Handler("commandBus")
    fun handle(envelope: Envelope<RemoveCartOffer>): Cart {

        val cart = carts.findOneByNumber(envelope.message.cartNumber) ?: throw CartNotFoundException(envelope.message.cartNumber)
        cart.remove(envelope.message.sku)
        carts.save(cart)

        try {
            eventBus.dispatch(CartOfferWasRemoved(cart.number, envelope.message.sku))
        } catch (ex: Throwable) {
            // todo log error here
            println(ex)
        }

        return cart
    }
}
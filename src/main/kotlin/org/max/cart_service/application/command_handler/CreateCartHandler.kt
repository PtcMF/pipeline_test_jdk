package org.max.cart_service.application.command_handler

import org.max.cart_service.application.command.CreateCart
import org.max.cart_service.application.factory.CartFactory
import org.max.cart_service.domain.entity.Cart
import org.max.cart_service.domain.event.CartWasCreated
import org.max.cart_service.domain.repository.CartRepository
import org.max.messenger.core.Envelope
import org.max.messenger.core.MessageBus
import org.max.messenger.core.annotation.Handler
import org.springframework.stereotype.Component

@Component
class CreateCartHandler (
    private val carts: CartRepository,
    private val factory: CartFactory,
    private val eventBus: MessageBus
) {

    @Handler("commandBus")
    fun handle(envelope: Envelope<CreateCart>): Cart {

        val cart = factory.create(envelope.message)
        carts.save(cart)

        try {
            eventBus.dispatch(CartWasCreated(cart.number))
        } catch (ex: Throwable) {
            // todo log error here
        }

        return cart
    }
}
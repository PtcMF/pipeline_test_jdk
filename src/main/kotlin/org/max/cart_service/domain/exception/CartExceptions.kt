package org.max.cart_service.domain.exception

open class ResourceNotFound(message: String) : Exception(message)

open class CartNotFoundException(number: String) : ResourceNotFound("Cart with number \"$number\" was not found")

open class CartOfferNotFoundException(sku: String) : ResourceNotFound("Offer with sku \"$sku\" was not found")

open class CartOfferExpiredException(sku: String) : ResourceNotFound("Offer with sku \"$sku\" was expired")

open class CartOfferDuplicatedException(sku: String) : ResourceNotFound("Offer with sku \"$sku\" is already in the cart")

open class CartOfferSchemaNotFoundException(cartNumber: String, schemaId: Long) : ResourceNotFound ("There is no schema with id \"$schemaId\" defined to cart \"$cartNumber\"")
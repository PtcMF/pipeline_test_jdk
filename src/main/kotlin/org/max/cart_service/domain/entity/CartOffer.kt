package org.max.cart_service.domain.entity

import org.max.cart_service.domain.exception.CartOfferSchemaNotFoundException
import org.hibernate.annotations.Fetch
import org.hibernate.annotations.FetchMode
import org.javamoney.moneta.Money
import java.time.LocalDateTime
import javax.money.MonetaryAmount
import javax.persistence.*

@Entity
class CartOffer(
    val sku: String,
    @ManyToOne (targetEntity = Cart::class)
    var cart: Cart,
    baseAmount: MonetaryAmount,
    adjustmentAmount: MonetaryAmount,
    totalAmount: MonetaryAmount,
    @Column(columnDefinition = "text")
    val products: String,
    @OneToMany(mappedBy = "offer", cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    val schemas: List<CartOfferSchema> = mutableListOf(),

    val availabilityStarts: LocalDateTime? = null,
    val availabilityEnds: LocalDateTime? = null
) {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null

    @Column(name = "baseAmount")
    private var _baseAmount: Long = 0
    val baseAmount: MonetaryAmount
        get() = Money.of(_baseAmount, cart.currency)

    @Column(name = "adjustmentAmount")
    private var _adjustmentAmount: Long = 0
    val adjustmentAmount: MonetaryAmount
        get() = Money.of(_adjustmentAmount, cart.currency)

    @Column(name = "totalAmount")
    private var _totalAmount: Long = 0
    val totalAmount: MonetaryAmount
        get() = Money.of(_totalAmount, cart.currency)

    @OneToMany(mappedBy = "offer", cascade = [CascadeType.ALL])
    val adjustments = mutableListOf<CartAdjustment>()

    val createdAt: LocalDateTime = LocalDateTime.now()

    init {
        _baseAmount = baseAmount.number.toInt().toLong()
        _adjustmentAmount = adjustmentAmount.number.toInt().toLong()
        _totalAmount = totalAmount.number.toInt().toLong()
        schemas.forEach { it.offer = this }
    }

    fun isExpired(): Boolean {
        return if (null == availabilityEnds) {
            false
        } else LocalDateTime.now().isAfter(availabilityEnds)
    }

    fun hasSchemaDefinition(schemaId: Long): Boolean {
        return schemas.any { it.id == schemaId }
    }

    fun setSchemaData(schemaId: Long, data: String) {
        val schema = schemas.find { it.id == schemaId } ?: throw CartOfferSchemaNotFoundException(sku, schemaId)
        schema.data = data
    }

    fun unsetSchemaData(schemaId: Long) {
        val schema = schemas.find { it.id == schemaId } ?: throw CartOfferSchemaNotFoundException(sku, schemaId)
        schema.data = null
    }

    fun getSchemaData(schemaId: Long): String? {
        val schema = schemas.find { it.id == schemaId } ?: throw CartOfferSchemaNotFoundException(sku, schemaId)
        return schema.data
    }
}

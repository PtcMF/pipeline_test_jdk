package org.max.cart_service.domain.entity

import org.javamoney.moneta.Money
import javax.money.MonetaryAmount
import javax.persistence.*

@Entity
class CartAdjustment(
    @ManyToOne(targetEntity = Cart::class) var cart: Cart? = null,
    @ManyToOne(targetEntity = CartOffer::class) var offer: CartOffer? = null,
    val code: String,
    amount: MonetaryAmount
) {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null

    @Column(name = "amount")
    private var _amount: Long = amount.number.toInt().toLong()
    val amount: MonetaryAmount
        get() = Money.of(_amount, cart?.currency ?: "BRL")
}

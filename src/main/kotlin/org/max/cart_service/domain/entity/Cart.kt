package org.max.cart_service.domain.entity

import org.max.cart_service.domain.exception.CartOfferDuplicatedException
import org.max.cart_service.domain.exception.CartOfferExpiredException
import org.max.cart_service.domain.exception.CartOfferNotFoundException
import org.javamoney.moneta.Money
import org.max.cart_service.domain.exception.CartOfferSchemaNotFoundException
import java.lang.Exception
import java.time.LocalDateTime
import javax.money.MonetaryAmount
import javax.persistence.*

@Entity
class Cart(
    @Column(unique = true) val number: String,
    @Transient val metadata: MutableMap<String, Any> = hashMapOf()
) {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0

    @Column(columnDefinition = "char(3)")
    val currency: String = "BRL"

    @Column(name = "baseAmount")
    private var _baseAmount: Long = 0
    val baseAmount: MonetaryAmount
        get() = Money.of(_baseAmount, currency)

    @Column(name = "adjustmentAmount")
    private var _adjustmentAmount: Long = 0
    val adjustmentAmount: MonetaryAmount
        get() = Money.of(_adjustmentAmount, currency)

    @Column(name = "totalAmount")
    private var _totalAmount: Long = 0
    val totalAmount: MonetaryAmount
        get() = Money.of(_totalAmount, currency)

    private var offersCount: Int = 0

    @OneToMany(mappedBy = "cart", cascade = [CascadeType.ALL], fetch = FetchType.EAGER, orphanRemoval = true)
    val offers = mutableListOf<CartOffer>()

    @OneToMany(mappedBy = "cart", cascade = [CascadeType.ALL])
    val adjustments = mutableListOf<CartAdjustment>()

    val createdAt: LocalDateTime = LocalDateTime.now()
    var updatedAt: LocalDateTime = LocalDateTime.now()
    var expiresAt: LocalDateTime? = null

    // @todo incluir coluna metadata

    fun add(
        sku: String,
        baseAmount: MonetaryAmount,
        adjustmentAmount: MonetaryAmount,
        totalAmount: MonetaryAmount,
        adjustments: List<CartAdjustment> = emptyList(),
        schemas: List<CartOfferSchema> = emptyList(),
        products: String,
        availabilityStarts: LocalDateTime? = null,
        availabilityEnds: LocalDateTime? = null
    ): CartOffer {

        if (this.has(sku)) {
            throw CartOfferDuplicatedException(sku)
        }

        val offer = CartOffer(
            cart = this,
            sku = sku,
            baseAmount = baseAmount,
            adjustmentAmount = adjustmentAmount,
            totalAmount = totalAmount,
            schemas = schemas,
            products = products,
            availabilityStarts = availabilityStarts,
            availabilityEnds = availabilityEnds
        )

        if (offer.isExpired()) {
            throw CartOfferExpiredException(sku)
        }

        adjustments.forEach {
            addAdjustment(it.amount, it.code, offer)
        }

        offers.add(offer)
        refresh()
        return offer
    }

    fun remove(sku: String) {
        val found = find(sku)
        adjustments.removeIf { adjustment -> adjustment.offer == found }
        offers.removeIf { offer -> offer == found }
        refresh()
    }

    fun find(sku: String): CartOffer {
        return offers.find { offer -> offer.sku == sku } ?: throw CartOfferNotFoundException(sku)
    }

    fun has(sku: String): Boolean {
        return offers.any { offer -> offer.sku == sku }
    }

    fun count(): Int = offers.size

    fun addAdjustment(amount: MonetaryAmount, code: String, offer: CartOffer? = null) {
        val adjustment = CartAdjustment(
            cart = this,
            offer = offer,
            amount = amount,
            code = code
        )

        adjustments.add(adjustment)
        refresh()
    }

    private fun refresh() {
        _baseAmount = 0
        _adjustmentAmount = 0
        _totalAmount = 0
        offersCount = offers.size

        offers.forEach {
            _baseAmount += it.baseAmount.number.toInt()
            _adjustmentAmount += it.adjustmentAmount.number.toInt()
            _totalAmount += it.totalAmount.number.toInt()
        }

        adjustments.forEach {
            if (null == it.offer) {
                _adjustmentAmount += it.amount.number.toInt()
                _totalAmount += it.amount.number.toInt()
            }
        }
    }

    fun set(name: String, value: Any) {
        metadata[name] = value
    }

    fun get(name: String): Any? {
        return metadata[name]
    }

    fun setSchemaData(schemaId: Long, data: String) {
        val offer = offers.firstOrNull { it.hasSchemaDefinition(schemaId) } ?: throw CartOfferSchemaNotFoundException(number, schemaId)
        offer.setSchemaData(schemaId, data) // todo SALVAR JSON OU STRING CODADA?
    }
}

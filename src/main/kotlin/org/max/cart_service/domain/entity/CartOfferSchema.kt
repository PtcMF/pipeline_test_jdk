package org.max.cart_service.domain.entity

import javax.persistence.*

@Entity
class CartOfferSchema(
    val name: String,
    @Column(columnDefinition = "text")
    val definition: String,
    var data: String? = null
) {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null

    @ManyToOne
    lateinit var offer: CartOffer
}

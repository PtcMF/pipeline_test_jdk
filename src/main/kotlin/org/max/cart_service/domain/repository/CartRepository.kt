package org.max.cart_service.domain.repository

import org.max.cart_service.domain.entity.Cart
import org.springframework.data.repository.CrudRepository

interface CartRepository : CrudRepository<Cart, Long> {
    fun findOneById(id: Long): Cart?
    fun findOneByNumber(number: String): Cart?
}
package org.max.cart_service.domain.event

import org.max.cart_service.domain.entity.CartOffer

data class CartOfferWasAdded(val cartNumber: String, val cartOffer: CartOffer) : Event()

package org.max.cart_service.domain.event

import java.time.LocalDateTime
import java.util.*

open class Event {
    val id: UUID = UUID.randomUUID()
    val date: LocalDateTime = LocalDateTime.now()
}
package org.max.cart_service.domain.event

data class CartWasCreated (val cartNumber: String) : Event()
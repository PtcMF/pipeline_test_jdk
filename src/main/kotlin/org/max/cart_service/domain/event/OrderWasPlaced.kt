package org.max.cart_service.domain.event

data class OrderWasPlaced (
    val cartNumber: String
)
package org.max.cart_service.domain.event

data class CartOfferWasRemoved(val cartNumber: String, val cartOfferSku: String) : Event()
